import React, { useState, useEffect, useContext } from "react";
import io from "socket.io-client";

let socket;

const SOCKET_API_URL = `${process.env.REACT_APP_SOCKET_URL}/user`;
// const SOCKET_API_URL = "wss://sparkborsa-backend.herokuapp.com/user";

const UserContext = React.createContext();

const UserContextProvider = ({ children }) => {
  const [userAccountCard, setUserAccountCard] = useState([]);
  const [userAccountUtil, setUserAccountUtil] = useState([]);
  const [userHomeCard, setUserHomeCard] = useState({});
  const [userLogs, setUserLogs] = useState([]);

  useEffect(() => {
    //console.log("Initial userContext Render.");
    socket = io.connect(SOCKET_API_URL, {
      auth: { token: localStorage.getItem("token") },
    });
    socket.emit(
      "Initial Data",
      {
        auth: { token: localStorage.getItem("token") },
      },
      (data) => {
        //console.log(data);
      }
    );
    socket.on("User Homecard", (data) => {
      setUserHomeCard(data?.success);
      // console.log("HomeCards : ", data);
    });
    socket.on("User Accountcard", (data) => {
      setUserAccountCard(data?.success);
      // console.log("AccountCards : ", data);
    });
    socket.on("User Accountutil", (data) => {
      setUserAccountUtil([...data?.success]);
      // console.log("AccountUtils : ", data);
    });
    socket.on("User Logs", (data) => {
      setUserLogs([...data?.success]);
      // console.log("Logs : ", data);
    });
    socket.on("connect_error", (err) => {
      //console.log("Error reason: ", err.message);
      localStorage.removeItem("token");
      window.location.reload();
    });
    return () => {
      socket.removeAllListeners();
    };
  }, []);

  return (
    <UserContext.Provider
      value={{
        userAccountCard,
        userAccountUtil,
        userHomeCard,
        userLogs,
        socket,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export const useUserContext = () => {
  return useContext(UserContext);
};

export { UserContext, UserContextProvider };
