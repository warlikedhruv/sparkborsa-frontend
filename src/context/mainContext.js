import React, { useState, useEffect, useContext } from "react";
import axios from "axios";
import jwt_decode from "jwt-decode";

const APIURL = process.env.REACT_APP_API_URL;

const MainContext = React.createContext();

// Custom hook useToken to check change in token
const useToken = () => {
  const getToken = () => {
    const userToken = localStorage.getItem("token");
    return userToken;
  };

  const [token, setToken] = useState(getToken());

  const saveToken = (userToken, setCurrentUser) => {
    localStorage.setItem("token", userToken);
    // Decode the token and set current user
    const decoded = jwt_decode(userToken);
    setCurrentUser(decoded);
    setToken(userToken);
  };

  return { token, setToken: saveToken };
};

// Logout functionality
const logout = () => {
  localStorage.removeItem("token");
  window.location.reload();
};

const MainContextProvider = ({ children }) => {
  const { token, setToken } = useToken();
  const [currentUser, setCurrentUser] = useState();

  useEffect(() => {
    if (localStorage.getItem("token") && !currentUser) {
      const decoded = jwt_decode(localStorage.getItem("token"));
      setCurrentUser(decoded);
    }
  }, [token]);

  // Function to send request to login and fetch token
  const fetchAndSetToken = async (username, password) => {
    //console.log("Making token request");
    return axios
      .post(APIURL + "/api/auth/signin", { username, password })
      .then((response) => {
        //console.log(response.data);
        setToken(response.data, setCurrentUser);
      });
  };

  //Function to send request to register a new user
  // const registerNewUser = async (data) => {
  //   return axios.post(APIURL + "/api/auth/signup", data).then((response) => {
  //     setToken(response.data);
  //     return response.data;
  //   });
  // };

  return (
    <MainContext.Provider
      value={{
        token,
        currentUser,
        setCurrentUser,
        logout,
        fetchAndSetToken,
      }}
    >
      {children}
    </MainContext.Provider>
  );
};

export const useMainContext = () => {
  return useContext(MainContext);
};

export { MainContext, MainContextProvider };
