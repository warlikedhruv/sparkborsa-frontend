import React, { useState, useEffect, useContext } from "react";
import io from "socket.io-client";

let socket;

const SOCKET_API_URL = `${process.env.REACT_APP_SOCKET_URL}/admin`;
// const SOCKET_API_URL = "wss://sparkborsa-backend.herokuapp.com/admin";

const AdminContext = React.createContext();

const AdminContextProvider = ({ children }) => {
  const [accountCards, setAccountCards] = useState([]);
  const [accountUtils, setAccountUtils] = useState([]);
  const [homeCards, setHomeCards] = useState([]);
  const [logs, setLogs] = useState([]);
  const [newUsersList, setNewUsersList] = useState([]);
  const [userAccountRequests, setUserAccountRequests] = useState([]);

  // useEffect function to make initial request and start socket listeners
  useEffect(() => {
    //console.log("Initial adminContext Render.");
    socket = io.connect(SOCKET_API_URL, {
      auth: { token: localStorage.getItem("token") },
    });
    socket.emit(
      "Initial Data",
      {
        auth: { token: localStorage.getItem("token") },
      },
      (data) => {
        //console.log(data);
      }
    );
    socket.on("HomeCards", (data) => {
      setHomeCards([...data?.success]);
      // console.log("HomeCards : ", data);
    });
    socket.on("AccountCards", (data) => {
      setAccountCards([...data?.success]);
      // console.log("AccountCards : ", data);
    });
    socket.on("AccountUtils", (data) => {
      setAccountUtils([...data?.success]);
      // console.log("AccountUtils : ", data);
    });
    socket.on("Logs", (data) => {
      setLogs([...data?.success]);
      // console.log("Logs : ", data);
    });
    socket.on("NewUsersList", (data) => {
      setNewUsersList(data?.success);
      // console.log("NewUsersList : ", data);
    });
    socket.on("UserAccountRequests", (data) => {
      setUserAccountRequests(data?.success);
    });
    socket.on("connect_error", (err) => {
      //console.log("Error reason: ", err.message);
      localStorage.removeItem("token");
      window.location.reload();
    });
    return () => {
      socket.removeAllListeners();
    };
  }, []);

  // This function filters the data for a single user from the data of all users using his id.
  const filterUserData = (id) => {
    const user_account_card = accountCards.find(
      (o) => o["account_number"] === id
    );
    const user_account_util = accountUtils.filter(
      (o) => o["account_number"] === id
    );
    const user_homecard = homeCards.find((o) =>
      o["user_accounts"].includes(id)
    );
    const user_logs = logs.filter((o) => o["account_number"] === id);
    return { user_account_card, user_account_util, user_homecard, user_logs };
  };

  return (
    <AdminContext.Provider
      value={{
        accountCards,
        accountUtils,
        homeCards,
        logs,
        newUsersList,
        filterUserData,
        userAccountRequests,
        socket,
      }}
    >
      {children}
    </AdminContext.Provider>
  );
};

export const useAdminContext = () => {
  return useContext(AdminContext);
};

export { AdminContext, AdminContextProvider };
