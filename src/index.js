import React from "react";
import ReactDom from "react-dom";

import "./index.css";
import App from "./App";
import "bootstrap/dist/css/bootstrap.css";

// Global Main Context
import { MainContextProvider } from "./context/mainContext";

ReactDom.render(
  <React.StrictMode>
    <MainContextProvider>
      <App />
    </MainContextProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
