import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { useMainContext } from "../context/mainContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faUser,
  faCog,
  faBars,
  faDoorOpen,
} from "@fortawesome/free-solid-svg-icons";

const Sidebar = () => {
  const { token, currentUser, logout } = useMainContext();

  if (token && currentUser?.role === "ADMIN") {
    // Load this sidebar if role is admin
    return (
      <Wrapper className="sidebar">
        <div className="top-logo icon-card">
          <div className="logo">
            <FontAwesomeIcon className="icon" icon={faBars} />
          </div>
        </div>
        <div className="menu icon-card">
          <ul>
            <li>
              <Link to={"/dashboard"}>
                <FontAwesomeIcon className="icon" icon={faHome} />
              </Link>
            </li>
            <li>
              <Link to={"/newusers"}>
                <FontAwesomeIcon className="icon" icon={faUser} />
              </Link>
            </li>
          </ul>
        </div>
        <div className="sidebar-footer icon-card">
          <div>
            <FontAwesomeIcon
              className="icon"
              icon={faDoorOpen}
              onClick={logout}
            />
          </div>
        </div>
      </Wrapper>
    );
  } else if (token && currentUser?.role === "USER") {
    // Load this sidebar, if role is user
    return (
      <Wrapper className="sidebar">
        <div className="top-logo icon-card">
          <div className="logo">
            <FontAwesomeIcon className="icon" icon={faBars} />
          </div>
        </div>
        <div className="menu icon-card">
          <ul>
            <li>
              <Link to={"/userDashboard"}>
                <FontAwesomeIcon className="icon" icon={faHome} />
              </Link>
            </li>
            <li>
              <Link to={"/userDashboard"}>
                <FontAwesomeIcon className="icon" icon={faCog} />
              </Link>
            </li>
          </ul>
        </div>
        <div className="sidebar-footer icon-card">
          <div>
            <FontAwesomeIcon
              className="icon"
              icon={faDoorOpen}
              onClick={logout}
            />
          </div>
        </div>
      </Wrapper>
    );
  } else {
    return (
      <Wrapper className="sidebar">
        <div className="top-logo icon-card">
          <div className="logo">
            <FontAwesomeIcon className="icon" icon={faBars} />
          </div>
        </div>
        <div className="menu icon-card">
          <ul>
            <li>
              <Link to={"/visitorDashboard"}>
                <FontAwesomeIcon className="icon" icon={faHome} />
              </Link>
            </li>
          </ul>
        </div>
        <div className="sidebar-footer icon-card">
          <div>
            <FontAwesomeIcon
              className="icon"
              icon={faDoorOpen}
              onClick={logout}
            />
          </div>
        </div>
      </Wrapper>
    );
  }
};

/* Some color info
bgcolor: #151A22
uppercolor: #2B2F39
fontcolor: #fffefb
yellowcolor: #ff9b02
bluecolor: #526acb
*/

const Wrapper = styled.div`
  background-color: #151a22;
  height: 100px;
  width: 100vw;
  padding: 1rem;
  display: grid;
  grid-template-columns: 12% 75% 12%;
  span {
    color: white;
  }
  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
  }
  a {
    text-decoration: none;
  }
  .icon {
    color: #fffefb;
    font-size: 1.5rem;
  }
  .icon-card {
    border-radius: 25px;
  }
  .top-logo {
    background-color: #2b2f39;
    display: grid;
    place-items: center;
  }
  .menu {
    background-color: #2b2f39;
    margin: 0 1rem;
    ul {
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      gap: 2rem;
    }
  }
  .sidebar-footer {
    background-color: #2b2f39;
    display: grid;
    place-items: center;
  }

  @media screen and (min-width: 804px) {
    position: fixed;
    top: 0;
    left: 0;
    width: 120px;
    height: 100vh;
    padding: 1rem;
    grid-template-columns: none;
    grid-template-rows: 8% 84% 8%;
    .menu {
      display: grid;
      place-items: start center;
      margin: 1rem 0;
      ul {
        height: auto;
        margin-top: 2rem;
        display: grid;
        place-items: center;
        gap: 2rem;
      }
    }
    .sidebar-footer {
      background-color: #2b2f39;
      display: grid;
      place-items: center;
    }
  }
`;

export default Sidebar;
