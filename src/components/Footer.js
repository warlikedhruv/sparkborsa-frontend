import { Link } from "react-router-dom";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCopyright
} from "@fortawesome/free-solid-svg-icons";

const Footer = () => {
  return (
    <Wrapper>
      <h2><FontAwesomeIcon icon={faCopyright}/> Powered By XLab Research</h2>
    </Wrapper>
  );
};

/* Some color info
bgcolor: #151A22
uppercolor: #2B2F39
fontcolor: #fffefb
yellowcolor: #ff9b02
bluecolor: #526acb
*/
const Wrapper = styled.div`
  width: 100%;
  background-color: #151a22;
  display: grid;
  place-items: center;
  position: absolute;
  bottom: 10px;
  h2 {
    text-align: center;
    color: white;
    font-weight: 300;
    font-size: 1.2rem;
  }
`;

export default Footer;
