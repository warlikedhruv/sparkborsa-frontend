import { useAdminContext } from "../../../context/adminContext";

const useCalculateData = () => {
  const { homeCards, accountCards, logs } = useAdminContext();

  // Functions
  const totalUsers = () => {
    return homeCards.length;
  };

  const totalAccounts = () => {
    let sum = homeCards.reduce(
      (sum, item) => sum + item["user_accounts"].length,
      0
    );
    return sum;
  };

  const totalOnlineAccounts = () => {
    let sum = accountCards.reduce((sum, item) => {
      if (item["status"] === "online") {
        sum += 1;
      }
      return sum;
    }, 0);
    return sum;
  };

  const totalBalance = () => {
    let sum = accountCards.reduce((sum, item) => {
      return sum + item["account_balance"];
    }, 0);
    return sum;
  };

  const totalCashBalance = () => {
    let sum = accountCards.reduce((sum, item) => {
      return sum + parseFloat(item["cash"]["$numberDecimal"]);
    }, 0.0);
    return sum;
  };

  const graph_data = ()=>{
    let gData = logs.find(o=>o["doc_id"]==="total_balance");
    let gData2;
    if (gData) {
      gData2 = gData.data.map((val) => {
        return {
          time: parseInt(JSON.stringify(val.ts).split(".")[0]),
          value: val.v,
        };
      });
    }
    return gData2;
  }

  const getAllData = () => {
    let totUsers = totalUsers();
    let totAccounts = totalAccounts();
    let totOnlineAccounts = totalOnlineAccounts();
    // let totOfflineAccounts = totAccounts - totOnlineAccounts;
    let totBalance = totalBalance();
    let totCashBalance = totalCashBalance();
    let totLockedBalance = totBalance - totCashBalance;
    let gData = graph_data();
    return {
      totUsers,
      totAccounts,
      totOnlineAccounts,
      // totOfflineAccounts,
      totBalance,
      totCashBalance,
      totLockedBalance,
      gData
    };
  };

  return { getAllData, totalAccounts };
};

export default useCalculateData;
