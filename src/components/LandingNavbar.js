import React, { useState } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

const LandingNavbar = () => {
  const [isNavCollapsed, setIsNavCollapsed] = useState(true);
  const handleNavCollapse = () => {
    setIsNavCollapsed(!isNavCollapsed);
    //console.log("isNavCollapsed: ", isNavCollapsed);
  };

  return (
    <Wrapper>
      <nav className="navbar navbar-expand-lg fixed-top">
        <a href="/" className="navbar-brand">
          <img src="https://i.ibb.co/dWm9yyY/Rectangle.png" alt="logo" />
        </a>

        <button
          className="custom-toggler navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#basic-navbar-nav"
          aria-controls="basic-navbar-nav"
          aria-expanded="false"
          aria-label="Toggle navigation"
          onClick={handleNavCollapse}
        >
          <span className="navbar-toggler-icon">
            <FontAwesomeIcon icon={faBars} />
          </span>
        </button>

        <div
          className={
            isNavCollapsed
              ? "collapse-custom navbar-collapse"
              : "navbar-collapse"
          }
          id="basic-navbar-nav"
        >
          <ul className="navbar-nav ms-auto">
            <a
              className="nav-item"
              href="#spark-engine"
              onClick={handleNavCollapse}
            >
              SPARK-ENGINE
            </a>
            <a className="nav-item" href="#bots" onClick={handleNavCollapse}>
              Bots & Indicators
            </a>
            <a
              className="nav-item"
              href="#scanners"
              onClick={handleNavCollapse}
            >
              SCANNERS<span>coming soon</span>
            </a>
            <a
              className="nav-item"
              href="#spark-apps"
              onClick={handleNavCollapse}
            >
              SPARK APPS<span>coming soon</span>
            </a>
            <a
              className="nav-item"
              href="#copytrading"
              onClick={handleNavCollapse}
            >
              COPYTRADING<span>coming soon</span>
            </a>
            <a
              className="nav-item"
              href="#about-us"
              onClick={handleNavCollapse}
            >
              ABOUT US
            </a>
            <Link to="/login">
              <button className="btn2">LOGIN</button>
            </Link>
            <Link to="/signup">
              <button className="btn2">SIGNUP</button>
            </Link>
          </ul>
        </div>
      </nav>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  font-family: "Astro";
  nav {
    padding: 20px 10px;
  }
  .navbar-brand {
    img {
      width: 200px;
    }
  }
  .navbar-toggler-icon {
    color: white;
  }
  #basic-navbar-nav {
    background-color: #2b3336;
  }
  .collapse-custom {
    display: none;
  }
  .navbar-nav {
    display: flex;
    align-items: center;
    margin-top: 10px;
    gap: 1rem;
    width: 100%;
    a {
      text-decoration: none;
      display: flex;
      flex-direction: column;
      span {
        color: orange;
        font-size: 0.7rem;
      }
    }
  }
  .nav-item {
    margin: 0 10px;
    width: 100%;
    text-align: center;
    color: white !important;
  }

  .btn2 {
    color: #2b3336;
    background: linear-gradient(to right, #6bff8e, #00919e);
    border: none;
    padding: 0.25rem 0.5rem;
    border-radius: 10px;
  }

  /* @media screen and (min-width: 804px) {
    background-color: black;
  }

  @media screen and (min-width: 992px) {
    background-color: #2b3336;
  } */

  @media screen and (min-width: 804px) {
  }

  @media screen and (min-width: 992px) {
    .collapse-custom {
      display: show;
    }
    nav {
      max-width: 1920px;
      margin: 0 auto;
      background-color: rgba(0,0,0,0.3);
    }
    .navbar-brand {
      img {
        max-width: 200px;
      }
    }
    #basic-navbar-nav {
      background-color: transparent;
    }
    .navbar-nav {
      display: flex;
      align-items: center;
      gap: 1rem;
    }
    .nav-item {
      font-size: 0.9rem;
      margin: 0 10px;
      color: white;
    }

    .btn2 {
      color: #2b3336;
      background: linear-gradient(to right, #6bff8e, #00919e);
      border: none;
      padding: 0.25rem 0.5rem;
      border-radius: 10px;
      width: fit-content;
    }
  }
`;

export default LandingNavbar;
