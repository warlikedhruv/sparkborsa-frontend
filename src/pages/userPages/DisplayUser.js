import React, { useState, useEffect, useRef } from "react";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Sidebar, Footer } from "../../components";
import { useUserContext } from "../../context/userContext";
import { createChart } from "lightweight-charts";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChartLine } from "@fortawesome/free-solid-svg-icons";

let refreshcount = 0;
let chart;
let areaSeries;

let currentUserAccountCard, currentUserAccountUtil;

const DisplayUser = () => {
  useEffect(() => {
    //console.log("refreshcount: ", ++refreshcount);
  });

  const { id } = useParams();
  const { userAccountCard, userAccountUtil, userHomeCard, userLogs } =
    useUserContext();

  const currentUserLogs = userLogs.filter((o) => o["account_number"] === id);

  const [histType, setHistType] = useState("order_history");
  const [histData, setHistData] = useState([]);
  const [push_notif, setPush_notif] = useState([]);
  const [open_posit, setOpen_posit] = useState([]);
  const [graph_data, setGraph_data] = useState([]);

  currentUserAccountCard = userAccountCard.find(
    (o) => o["account_number"] === id
  );

  useEffect(() => {
    let temp = currentUserLogs.find(
      (o) => o["doc_id"] === "push_notifications"
    );
    setPush_notif(temp ? temp["data"] : []);
  }, [currentUserLogs]);

  useEffect(() => {
    currentUserAccountUtil = userAccountUtil.filter(
      (o) => o["account_number"] === id
    );
    // if (currentUserAccountUtil) {
    let temp = currentUserAccountUtil.find(
      (o) => o["doc_id"] === "open_positions"
    );
    temp = temp ? temp["data"] : [];
    setOpen_posit(
      temp.filter(
        (item) => item.symbol !== "BNBUSDT" && item.symbol !== "USDTUSDT"
      )
    );

    let gData = currentUserAccountUtil.find(
      (o) => o["doc_id"] === "account_chart"
    );
    let gData2 = gData.data.map((val) => {
      // let ts = JSON.stringify(val.ts * 1000);
      // ts = ts.split(".")[0];
      // let d = new Date(parseInt(ts));
      // d = JSON.stringify(d);
      // d = d.slice(1, 11);
      return {
        time: parseInt(JSON.stringify(val.ts).split(".")[0]),
        value: val.v,
      };
    });
    //console.log(gData2);
    setGraph_data(gData2);
    // }
  }, []);

  useEffect(() => {
    if (areaSeries) {
      areaSeries.setData(graph_data);
    }
  });

  useEffect(() => {
    var temp = currentUserLogs?.find((o) => o["doc_id"] === histType);
    temp = temp ? temp["data"] : [];
    setHistData(temp);
  }, [histType, currentUserLogs]);

  // Reference hook to get size of chart div
  const chartRef = useRef();

  useEffect(() => {
    var chart = createChart(chartRef.current, {
      width: chartRef?.current?.clientWidth - 15,
      height: chartRef?.current?.clientHeight - 15,
      layout: {
        backgroundColor: "#000000",
        textColor: "#d1d4dc",
      },
      rightPriceScale: {
        scaleMargins: {
          top: 0.3,
          bottom: 0.25,
        },
      },
      crosshair: {
        vertLine: {
          width: 5,
          color: "rgba(224, 227, 235, 0.1)",
          style: 0,
        },
        horzLine: {
          visible: false,
          labelVisible: false,
        },
      },
      timeScale: {
        timeVisible: true,
      },
      grid: {
        vertLines: {
          color: "rgba(42, 46, 57, 0)",
        },
        horzLines: {
          color: "rgba(42, 46, 57, 0)",
        },
      },
    });

    areaSeries = chart.addAreaSeries({
      lineWidth: 2,
      crossHairMarkerVisible: false,
    });
    //console.log("When running the graph:", graph_data);
    // Setting resize observer for chart Containers, lets test again, agin
    const ro = new ResizeObserver(() => {
      const elemWidth = chartRef?.current?.clientWidth;
      const elemHeight = chartRef?.current?.clientHeight;
      if (elemWidth && elemHeight) {
        chart.resize(elemWidth - 15, elemHeight - 15);
      }
    });
    ro.observe(chartRef.current);

    return () => {
      ro.disconnect();
    };
  }, []);

  return (
    <Wrapper>
      <Sidebar />
      <div className="main">
        <div style={{ marginLeft: "2rem" }} className="heading-section">
          <h4> Welcome to SparkBorsa</h4>
          <div className="btn-container">
            <Link to={`/userAccounts/${id}/payFees`}>
              <button className="btn btn-info">Pay Fees</button>
            </Link>
          </div>
        </div>
        <section className="top-section">
          <article
            className="graph card"
            ref={chartRef}
            id="chart-container"
          ></article>
          <article className="info">
            <div className="first-info card info-card">
              <h4>{userHomeCard?.name || "undefined"}</h4>
              <p>
                $
                {parseFloat(currentUserAccountCard?.account_balance).toFixed(
                  4
                ) || "0"}
              </p>
            </div>
            <div className="second-info card info-card">
              <div>
                <h4>Total Lockdown</h4>
                <p>${userHomeCard?.total_lockdown || 0}</p>
              </div>
              <FontAwesomeIcon className="icon" icon={faChartLine} />
            </div>
            <div className="third-info card info-card">
              <article className="info-box">
                <h5>Open trades</h5>
                <p>{open_posit.length}</p>
              </article>
              <article className="info-box">
                <h5>Free balance</h5>
                <p>
                  $
                  {currentUserAccountCard?.cash &&
                    (parseFloat(
                      currentUserAccountCard.cash["$numberDecimal"]
                    ).toFixed(2) ||
                      0)}
                </p>
              </article>
              <article className="info-box">
                <h5>Profit share</h5>
                <p>
                  $
                  {parseFloat(
                    currentUserAccountCard?.account_balance -
                      currentUserAccountCard?.last_round_start_balance
                  ).toFixed(2)}
                </p>
              </article>
              <article className="info-box">
                <h5>Locked balance</h5>
                <p>
                  $
                  {currentUserAccountCard?.cash &&
                    parseFloat(
                      currentUserAccountCard?.account_balance -
                        currentUserAccountCard?.cash["$numberDecimal"]
                    ).toFixed(2)}
                </p>
              </article>
            </div>
          </article>
          <article className="positions card">
            <h5>Open Positions</h5>
            <div className="positions-table">
              <section className="t-head">
                <article>Symbol</article>
                <article>Value</article>
                <article>Position Size</article>
                <article className="pnl">PNL</article>
              </section>
              {open_posit.map((item, index) => {
                return (
                  <section className="t-row" key={index}>
                    <article>{item["symbol"]}</article>
                    <article>
                      {parseFloat(item["freeValue"]).toFixed(3)}
                    </article>
                    <article>
                      {parseFloat(item["positionSize"]).toFixed(3)}
                    </article>
                    <article
                      className={
                        item["profitLossPercent"] < 0
                          ? "pnl loss"
                          : "pnl profit"
                      }
                    >
                      {parseFloat(item["profitLossPercent"]).toFixed(2)}%
                    </article>
                  </section>
                );
              })}
            </div>
          </article>
          <article className="alerts card">
            <h5>Alerts</h5>
            <div className="alert-list">
              {push_notif &&
                push_notif.map((item, index) => {
                  return (
                    <p className="alert card" key={index}>
                      {item["msg"]}
                    </p>
                  );
                })}
            </div>
          </article>
        </section>
        <section className="history-section">
          <article className="history card">
            <div className="filter-btns">
              <button
                className="order-hist hist-btn"
                onClick={() => {
                  setHistType("order_history");
                }}
              >
                Order history
              </button>
              <button
                className="lockdown-hist hist-btn"
                onClick={() => {
                  setHistType("lockdown_log");
                }}
              >
                All Lockdown
              </button>
              <button
                className="trades-hist hist-btn"
                onClick={() => {
                  setHistType("trades_hist");
                }}
              >
                Trades Journal
              </button>
            </div>
            <div className="data">
              {histData.map((item, index) => {
                return (
                  <div className="data-row" key={index}>
                    <article className="time card">
                      {JSON.stringify(
                        new Date(
                          parseInt(JSON.stringify(item.t * 1000).split(".")[0])
                        )
                      ).slice(1, 20)}
                    </article>
                    <article className="msg card">
                      {item.msg || item.s + " and value " + item.v}
                    </article>
                  </div>
                );
              })}
            </div>
          </article>
        </section>
        <Footer className="footer" />
      </div>
    </Wrapper>
  );
};

/* Some color info
bgcolor: #151A22
uppercolor: #2B2F39
fontcolor: #fffefb
yellowcolor: #ff9b02
bluecolor: #526acb
*/
const Wrapper = styled.div`
  position: relative;
  min-height: 100vh;
  background-color: #151a22;
  color: #fffefb;
  .main {
    padding: 1rem 1rem 1rem 1rem;
    position: relative;
    .heading-section {
      display: flex;
      flex-direction: column;
      align-items: center;
      .btn-container {
        padding-right: 1rem;
        display: flex;
        gap: 1rem;
        button {
          font-weight: 500;
          color: black;
          width: fit-content;
        }
      }
    }
    .top-section {
      padding: 1rem 0;
      display: grid;
      gap: 1rem;
    }
    .card {
      background-color: #2b2f39;
      border-radius: 25px;
    }
    .graph {
      display: grid;
      place-items: center;
      background-color: black;
      height: 300px;
    }
    .info {
      display: flex;
      flex-direction: column;
      gap: 1rem;
      .info-card {
        padding: 1rem;
        p {
          margin: 0;
        }
      }
      h4 {
        font-weight: 400;
        font-size: 1.3rem;
      }
      .first-info {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        border-left: 4px solid red;
        h4 {
          padding: 5px;
          width: fit-content;
          margin: 0;
        }
        p {
          margin-top: 10px;
          font-size: 1.5rem;
          padding: 5px 10px;
          color: white;
          background-color: #151a22;
          border-radius: 10px;
          width: fit-content;
        }
      }
      .second-info {
        border-left: 4px solid #ff9b02;
        display: grid;
        grid-template-columns: 2fr 1fr;
        p {
          font-size: 1.5rem;
          font-weight: 500;
          color: #ff9b02;
        }
        .icon {
          place-self: center;
          font-size: 3rem;
          color: lightgreen;
        }
      }
      .third-info {
        border-left: 4px solid #526acb;
        display: grid;
        place-items: center left;
        grid-template-columns: 1fr 1fr;
        h5 {
          /* text-align: center; */
        }
        p {
          margin: 0 0 5px 0;
          text-align: center;
          font-size: 1.2rem;
          color: #526acb;
        }
      }
    }
    .positions {
      padding: 1rem;
      margin: 0;
      .card {
        border-radius: 10px;
        padding: 5px 7px;
      }
      .positions-table {
        padding: 5px;
        display: grid;
        gap: 7px;
        max-height: 500px;
        overflow-y: auto;
        ::-webkit-scrollbar {
          width: 0px;
        }
        section {
          display: grid;
          grid-template-columns: repeat(4, 1fr);
          background-color: #151a22;
          padding: 10px;
          border-radius: 10px;
        }
        .t-head {
          color: #526acb;
          padding-bottom: 10px;
          border-bottom: 5px solid #151a22;
          background-color: #2b2f39;
          display: flex;
          flex-direction: column;
          text-align: center;
        }
        .t-row {
          display: flex;
          flex-direction: column;
          text-align: center;
        }
        .pnl {
          text-align: center;
        }
        .profit {
          color: green;
        }
        .loss {
          color: red;
        }
      }
    }
    .alerts {
      padding: 1rem;
      .card {
        border-radius: 10px;
        padding: 5px 7px;
        margin-bottom: 7px;
      }
      .alert-list {
        max-height: 500px;
        overflow-y: auto;
        ::-webkit-scrollbar {
          width: 0px;
        }
        .alert {
          background-color: #151a22;
          color: #fffefb;
        }
      }
    }

    /* history section */
    .history {
      font-weight: 500;
      margin-top: 1rem;
      padding: 10px;
      margin-bottom: 5rem;
      .filter-btns {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 1rem;
        .hist-btn {
          background-color: #151a22;
          border: none;
          border-radius: 10px;
          color: #fffefb;
          padding: 10px;
        }
        .order-hist {
          border-left: 5px solid #526acb;
        }
        .lockdown-hist {
          border-left: 5px solid lightgreen;
        }
        .trades-hist {
          border-left: 5px solid lightgray;
        }
      }
      .data {
        margin: 25px 0;
        display: flex;
        flex-direction: column;
        gap: 1rem;
        max-height: 70vh;
        overflow-y: auto;
        ::-webkit-scrollbar {
          width: 0px;
        }
        .data-row {
          display: grid;
          gap: 5%;
          .time {
            text-align: center;
            background-color: #151a22;
            padding: 10px;
            border-radius: 10px;
          }
          .msg {
            background-color: #151a22;
            padding: 10px 20px;
            border-radius: 10px;
          }
        }
      }
    }
  }

  @media screen and (min-width: 804px) {
    .main {
      margin-left: 120px;
      padding: 2rem 2rem 1rem 0;
      .heading-section {
        flex-direction: row;
        align-items: auto;
        justify-content: space-between;
      }
      .top-section {
        grid-template-columns: 64% 34%;
      }
      .card {
        background-color: #2b2f39;
        border-radius: 25px;
      }
      .graph {
        height: auto;
        background-color: black;
      }
      .positions {
        .positions-table {
          .t-head {
            display: grid;
          }
          .t-row {
            display: grid;
          }
        }
      }

      .history {
        padding: 20px 40px;
        margin-bottom: 3rem;

        .filter-btns {
          display: flex;
          flex-direction: row;
          justify-content: space-between;
          .hist-btn {
            width: 25%;
          }
        }
        .data {
          .data-row {
            grid-template-columns: 25% 70%;
          }
        }
      }
    }
  }

  @media screen and (min-width: 992px) {
    * {
      max-width: 1920px;
    }
  }
`;

export default DisplayUser;
