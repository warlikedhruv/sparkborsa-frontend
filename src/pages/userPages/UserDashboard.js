import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Sidebar, Footer } from "../../components";
import { useUserContext } from "../../context/userContext";
import img from "../../assets/images/band.png";

const UserDashboard = (props) => {
  const { userHomeCard } = useUserContext();
  const [message, setMessage] = useState({ type: "success", text: "" });

  useEffect(() => {
    if (props?.location?.state) {
      //console.log(props);
      setMessage({ type: "success", text: props?.location?.state?.message });
    }
  }, []);

  return (
    <Wrapper>
      <Sidebar />
      <div className="main">
        <div className="top-container">
          {message?.text && (
            <div
              className={
                message?.type === "warning"
                  ? "alert alert-danger"
                  : "alert alert-success"
              }
              role="alert"
            >
              {message?.text}
            </div>
          )}
          <div className="card display-container">
            <div className="text-data">
              <h3>Welcome to Sparkborsa</h3>
              <p>Improve Your Balance With Our Smart Tools</p>
            </div>
            <div className="img-container moveUpDown">
              <img
                src="https://i.ibb.co/PgkxPpc/s-1.png"
                alt="Bitcoin flowchart"
              />
            </div>
          </div>
          <div className="btn-container">
            <Link to="/addAnotherAccount">
              <button className="btn">Add Account</button>
            </Link>
          </div>
        </div>
        <div className="accounts-container">
          {/* <div className="moving-image-container">
            <div className="container1"></div>
          </div> */}
          {userHomeCard?.user_accounts &&
            userHomeCard?.user_accounts?.map((account) => {
              return <AccountCard account={account} key={account} />;
            })}
        </div>
        <Footer className="footer" />
      </div>
    </Wrapper>
  );
};

const AccountCard = ({ account }) => {
  const { userAccountCard } = useUserContext();
  const accountData = userAccountCard.find(
    (o) => o["account_number"] === account
  );
  let apiStatus = accountData?.["apistatus"]==="valid";
  return (
    <div className="">
      {!apiStatus && (
        <div
          className="alert alert-danger"
          role="alert"
        >
          The permissions of your api key have expired. According to the rules of Binance, the permission to trade in spot and fiat expires after 90 days. To enable the trading permissions you need to visit your binance account and enable the permissions again.
        </div>
      )}
      <section className="account-card card">
        <article>
          <h5>Account Number</h5>
          <p>{account}</p>
        </article>
        <article>
          <h5>Balance</h5>
          <p>${parseFloat(accountData?.account_balance).toFixed(2)}</p>
        </article>
        <article>
          <h5>API Status</h5>
          <p>{apiStatus ? "Valid" : "Expired"}</p>
          {!apiStatus && <p className="expired-link">Update Permissions</p>}
        </article>
        <Link to={`/userAccounts/${account}`}>
          <article>
            <p>View Profit Statistics</p>
          </article>
        </Link>
      </section>
    </div>
  );
};

/* Some color info
bgcolor: #151A22
uppercolor: #2B2F39
fontcolor: #fffefb
yellowcolor: #ff9b02
bluecolor: #526acb
*/

const Wrapper = styled.div`
  position: relative;
  background-color: #151a22;
  .main {
    min-height: 100vh;
    position: relative;
    .top-container {
      padding: 30px 15px;
      .display-container {
        background: #2b2f39;
        height: 200px;
        position: relative;
        margin-bottom: 30px;
        .text-data {
          text-align: center;
          padding-top: 2rem;
          h3 {
            font-family: "Moonhouse";
            color: white;
            font-size: 1.5rem;
            margin-bottom: 2rem;
          }
          p {
            color: white;
            font-weight: 500;
            font-size: 1.2rem;
            max-width: 300px;
            margin: 0 auto;
          }
        }
        .img-container {
          display: none;
        }
      }
      .btn-container {
        display: flex;
        justify-content: center;
      }
    }

    /* .moveLeftRight {
      animation-name: moveLR;
      animation-iteration-count: infinite;
      animation-direction: left;
      animation-duration: 10s;
    } */

    /* @keyframes moveLR {
      from {
        transform: translateX(0px);
        left: 0%;
      }
      to {
        transform: translateX(20px); 
        left: -50%;
      }
    } */

    /* #container2 {
      height: 100px;
      position: relative;
      overflow: hidden;
    } */

    /* .photobanner {
      position: absolute;
      top: 0px;
      left: 0px;
      overflow: hidden;
      white-space: nowrap;
      animation: bannermove 10s linear infinite;
    }
    */

    /* @keyframes bannermove {
      0% {
        transform: translate(0, 0);
      }
      100% {
        transform: translateY(100%, 0);
      }
    }  */
    /* ---------------------------------------------- */

    .accounts-container {
      padding: 0px 15px 75px 15px;
      display: grid;
      gap: 1rem;
      width: 100%;
      .moving-image-container {
        height: 100px;
        /* max-width: 80%; */
        /* overflow: hidden; */
        /* width: 100%; */
        background-image: url(${img});
        background-size: contain;
        background-repeat: repeat;
        background-position: center;
        /* position: relative; */
        /* overflow: hidden; */
        /* .container1 {
          height: 100px;
          width: 80%;
          position: absolute;
          background: orange;
          display:flex;
          overflow: hidden;
          left: 80%;
          img {
            height: 100%;
          }
        } */
        /* .container2 {
          position: absolute;
          top:0;
          background:orange;
          left:80%;
          height: 100px;
          width: 100%;
          img {
            height: 100%;
          } */
        /* } */
      }
      .account-card {
        display: flex;
        flex-direction: column;
        gap: 1rem;
        justify-content: space-between;
        text-align: center;
        align-items: center;
        color: white;
        padding: 25px 50px;
        p {
          margin: 0;
          font-weight: 500;
        }
        a {
          text-decoration: none;
          p {
            color: green;
          }
        }
        .expired-link {
          color: red;
        }
      }
    }
  }
  .card {
    background-color: #2b2f39;
    border-radius: 25px;
  }
  .btn {
    width: 200px;
    background-color: #2b2f39;
    border: none;
    border-radius: 10px;
    color: #fffefb;
    padding: 15px 0;
    font-weight: 500;
  }

  @media screen and (min-width: 804px) {
    .main {
      margin-left: 120px;
      .top-container {
        padding: 75px 50px;
        .display-container {
          height: 300px;
          margin-bottom: 80px;
          .text-data {
            max-width: 40%;
            position: absolute;
            bottom: 30px;
            left: 5%;
          }
          .img-container {
            display: flex;
            max-width: 50%;
            position: absolute;
            right: 5%;
            top: -50px;
            img,
            svg {
              max-width: 100%;
              max-height: 400px;
            }
          }
        }
        .btn-container {
          display: flex;
          float: right;
        }
      }
      .accounts-container {
        padding: 0px 50px 75px 50px;
        display: grid;
        gap: 1rem;
        .account-card {
          display: flex;
          flex-direction: row;
          gap: 1rem;
          justify-content: space-between;
          text-align: center;
          align-items: center;
          color: white;
          padding: 25px 50px;
          p {
            margin: 0;
            font-weight: 500;
          }
          a {
            text-decoration: none;
            p {
              color: green;
            }
          }
        }
      }
    }
    .card {
      background-color: #2b2f39;
      border-radius: 25px;
    }
    .btn {
      width: 200px;
      background-color: #2b2f39;
      border: none;
      border-radius: 10px;
      color: #fffefb;
      padding: 15px 0;
      font-weight: 500;
    }
  }

  @media screen and (min-width: 992px) {
    * {
      max-width: 1920px;
    }
  }
`;

export default UserDashboard;
