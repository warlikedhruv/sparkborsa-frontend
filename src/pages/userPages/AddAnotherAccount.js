import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Sidebar, Footer } from "../../components";
import { Puff } from "react-loading-icons";
import { Redirect, Link } from "react-router-dom";
import { useUserContext } from "../../context/userContext";

const AddAnotherAccount = () => {
  const { socket } = useUserContext();
  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const [status, setStatus] = useState({ type: "warning", text: "" });
  const [showButton, setShowButton] = useState(false);
  const [data, setData] = useState({
    apiKey: "",
    apiSecret: "",
    exchange: "binance.com",
    strategy: "STRI Strategy",
    telegramUsername: "",
    risk: 1,
  });

  useEffect(() => {
    socket.emit(
      "Fetch User Account Requests",
      {
        auth: { token: localStorage.getItem("token") },
      },
      (res) => {
        //console.log("response:", res);
        if (res.status === 200) {
          if (res.reqStatus === "applied") {
            setStatus({
              type: "success",
              text: "Your previous account request is still under process. You will be notified her if the request is rejected. If the request is approved then the account will be automatically reflected in the dashboard. You need to relogin to access the new account if it is approved.",
            });
          } else {
            setShowButton(true);
            setStatus({
              type: "warning",
              text: "Your previous account request has been rejected.",
            });
          }
        } else {
        }
      }
    );
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    setMessage("");
    setLoading(true);

    if (
      !data.apiKey ||
      !data.apiSecret ||
      !data.exchange ||
      !data.strategy ||
      !data.telegramUsername ||
      !data.risk
    ) {
      setMessage("All fields are mandatory!");
      //console.log(data);
    } else {
      // check api key and secret
      const temp = { apiKey: data.apiKey, apiSecret: data.apiSecret, exchangeType:data.exchange };
      await socket.emit(
        "Verify Binance Key",
        {
          auth: { token: localStorage.getItem("token") },
          data: temp,
        },
        (res) => {
          //console.log(res.status);
          if (res.status === 200) {
            console.log(res);
            socket.emit(
              "New User Account Request",
              {
                auth: { token: localStorage.getItem("token") },
                data: data,
                balance: res.totBalance,
              },
              (res) => {
                if (res.status === 200) {
                  setRedirect(true);
                } else {
                  //console.log(data);
                }
              }
            );
          } else {
            setMessage(res.msg);
          }
        }
      );
    }
    setLoading(false);
  };

  const messageReceivedHandler = () => {
    socket.emit("Remove User Account Request", {
      auth: { token: localStorage.getItem("token") },
    });
    setShowButton(false);
    window.location.reload();
  };

  return (
    <Wrapper>
      {redirect && (
        <Redirect
          to={{
            pathname: "/userDashboard",
            state: {
              message: "A new user account request has been placed",
            },
          }}
        />
      )}
      <Sidebar />
      <div className="main">
        {status?.text && (
          <div
            className={
              status?.type === "warning"
                ? "alert alert-danger"
                : "alert alert-success"
            }
            role="alert"
          >
            {status?.text}
            {showButton && (
              <button className="btn" onClick={messageReceivedHandler}>
                Message Received
              </button>
            )}
          </div>
        )}
        <div className="top-container">
          <form onSubmit={handleSubmit}>
            {message && (
              <div className="message-container">
                <div className="alert alert-danger" role="alert">
                  {message}
                </div>
              </div>
            )}
            {loading && (
              <div className="loading-container">
                <Puff stroke="#ffffff" />
              </div>
            )}
            <label>
              <p>API Key</p>
              <input
                type="text"
                onChange={(e) => {
                  let temp = data;
                  temp.apiKey = e.target.value;
                  setData(temp);
                }}
              />
            </label>
            <label>
              <p>API Secret</p>
              <input
                type="text"
                onChange={(e) => {
                  let temp = data;
                  temp.apiSecret = e.target.value;
                  setData(temp);
                }}
              />
            </label>
            <label>
              <p>Exchange</p>
              <select
                name="exchange"
                id="exchange"
                onChange={(e) => {
                  let temp = data;
                  temp.exchange = e.target.value;
                  setData(temp);
                }}
              >
                <option selected value="binance.com">
                  Binance
                </option>
                <option value="binance.us">Binance.us</option>
                <option value="kucoin.com">Kucoin (Not Available)</option>
              </select>
            </label>
            <label>
              <p>Strategy</p>
              <select
                name="strategy"
                id="strategy"
                onChange={(e) => {
                  let temp = data;
                  temp.strategy = e.target.value;
                  setData(temp);
                }}
              >
                <option value="STRI Strategy">STRI Strategy</option>
              </select>
            </label>
            <label>
              <p>Telegram Username</p>
              <input
                type="text"
                onChange={(e) => {
                  let temp = data;
                  temp.telegramUsername = e.target.value;
                  setData(temp);
                }}
              />
            </label>
            <label>
              <p>Risk</p>
              {/* A slider for low, medium or high database */}
              <div>
                <input
                  type="range"
                  name="risk"
                  id="risk"
                  list="risk-breakpoints"
                  value="1"
                  disabled
                  min={1}
                  max={3}
                  onChange={(e) => {
                    let temp = data;
                    temp.risk = e.target.value;
                    setData(temp);
                  }}
                />
                <datalist className="risk-breakpoints">
                  <option value="1" label="LOW"></option>
                  <option value="2" label="MED"></option>
                  <option value="3" label="HIGH"></option>
                </datalist>
              </div>
            </label>
            <button className="btn" type="submit">
              Add Account
            </button>
            <h6>
              By creating an account on this site you are agreeing to it's{" "}
              <Link to="/agreement">
                <span>Terms and Condition's</span>
              </Link>
            </h6>
          </form>
          <img
            src="https://i.ibb.co/fqbFLfb/cryptocurrency-v1.png"
            alt="People buying bitcoins"
            className="moveUpDown"
          />
        </div>
        <Footer className="footer" />
      </div>
    </Wrapper>
  );
};

/* Some color info
bgcolor: #151A22
uppercolor: #2B2F39
fontcolor: #fffefb
yellowcolor: #ff9b02 
bluecolor: #526acb
*/

const Wrapper = styled.div`
  background-color: #151a22;
  .main {
    position: relative;
    min-height: 100vh;
    .top-container {
      padding: 15px;
      img {
        display: none;
      }
      form {
        display: flex;
        flex-direction: column;
        row-gap: 1.5rem;
        padding-top: 3rem;
        margin-bottom: 5rem;
        width: 100%;
        label {
          display: grid;
          column-gap: 1rem;
          row-gap: 0.5rem;
          text-align: left;
          align-items: center;
          p {
            margin: 0;
            text-align: center;
            font-weight: 500;
            font-size: 1.1rem;
            color: white;
          }
          input,
          select {
            min-width: 200px;
            border: none;
            border-radius: 10px;
            width: 100%;
            text-align: center;
            margin: 0 auto;
            padding: 0.5rem 0.5rem;
            background: #2b2f39;
            color: white;
          }
          select {
            width: 100%;
          }
        }
        datalist {
          display: flex;
          margin: 0 auto;
          color: white;
          justify-content: space-between;
        }
        h6 {
          text-align: center;
          color: grey;
          span {
            color: orange;
          }
        }
      }
    }
  }
  .card {
    background-color: #2b2f39;
    border-radius: 25px;
  }
  .btn {
    width: 200px;
    background-color: #2b2f39;
    border: none;
    border-radius: 10px;
    color: #fffefb;
    padding: 15px 0;
    font-weight: 500;
    margin: 0 auto;
  }

  @media screen and (min-width: 804px) {
    .main {
      margin-left: 120px;
      .top-container {
        min-height: 100vh;
        padding: 50px 50px 20px 50px;
        display: grid;
        grid-template-columns: 1fr 1fr;
        justify-content: space-between;
        align-items: center;
        img {
          display: inline-block;
          max-width: 100%;
          height: auto;
          align-self: flex-start;
        }
        form {
          margin-bottom: 2rem;

          label {
            grid-template-columns: 1fr 1.5fr;
          }
        }
      }
    }
  }

  @media screen and (min-width: 992px) {
    * {
      max-width: 1920px;
    }
  }
`;

export default AddAnotherAccount;
