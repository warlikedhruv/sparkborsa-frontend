import React, { useState, useEffect } from "react";
import { useParams } from "react-router";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Sidebar, Footer } from "../../components";
import { useUserContext } from "../../context/userContext";

let currentUserAccountCard;
let currentUserAccountUtil;

const PayFees = (props) => {
  const { userHomeCard, userAccountCard } = useUserContext();
  const [message, setMessage] = useState({ type: "success", text: "" });

  const { id } = useParams();

  const currentUserAccountCard = userAccountCard.find(
    (o) => o["account_number"] === id
  );

  //console.log("UserAccountCard: ", currentUserAccountCard);

  const profit =
    currentUserAccountCard?.account_balance -
    currentUserAccountCard?.last_round_start_balance;
  const profitPerc =
    (profit * 100) / currentUserAccountCard?.last_round_start_balance;
  const feesAmount = profitPerc >= 12 ? (28 * profit) / 100 : 0;

  return (
    <Wrapper>
      <Sidebar />
      <div className="main">
        <div className="top-container">
          {message?.text && (
            <div
              className={
                message?.type === "warning"
                  ? "alert alert-danger"
                  : "alert alert-success"
              }
              role="alert"
            >
              {message?.text}
            </div>
          )}
          <div className="heading">
            <h2>Fees Payment</h2>
            <button className="btn btn-primary">Info</button>
          </div>
          <div className="form-container">
            <form>
              <label>
                <p>Account Id</p>
                <p className="value">{id}</p>
              </label>
              <label>
                <p>Boarding Amount</p>
                <p className="value">
                  ${currentUserAccountCard?.last_round_start_balance}
                </p>
              </label>
              <label>
                <p>Balance</p>
                <p className="value">
                  ${currentUserAccountCard?.account_balance}
                </p>
              </label>
              <label>
                <p>Profit</p>
                <p className="value">${profit}</p>
              </label>
              <label>
                <p>Profit Percent</p>
                <p className="value">{parseFloat(profitPerc).toFixed(2)}%</p>
              </label>
              <label>
                <p>Fees</p>
                <p className="value">${parseFloat(feesAmount).toFixed(2)}</p>
              </label>
              <label>
                <p>Start Date</p>
                <p className="value">
                  {currentUserAccountCard?.last_cut_off_date}
                </p>
              </label>
              {/* <button className="btn btn-primary pay-btn" onClick={handlePayment}>Pay</button> */}
            </form>
            {/* CoinPayments Button */}
            {currentUserAccountCard?.status === "offline" && (
              <form
                action="https://www.coinpayments.net/index.php"
                method="post"
              >
                <input type="hidden" name="cmd" value="_pay_simple" />
                <input type="hidden" name="reset" value="1" />
                <input
                  type="hidden"
                  name="merchant"
                  value="a32cf00442736ce98ab9645d438841af"
                />
                <input type="hidden" name="item_name" value="Sparkborsa Fees" />
                <input
                  type="hidden"
                  name="item_desc"
                  value={`Fees payment for account number ${id}`}
                />
                <input
                  type="hidden"
                  name="item_number"
                  value={`${id}-${new Date().getTime()}`}
                />
                <input
                  type="hidden"
                  name="invoice"
                  value={`invoice-${new Date().getTime()}`}
                />
                <input type="hidden" name="currency" value="USDT.TRC20" />
                <input type="hidden" name="amountf" value={feesAmount} />
                <input type="hidden" name="want_shipping" value="0" />
                <input
                  type="hidden"
                  name="success_url"
                  value="http://www.sparkborsa.com/userDashboard"
                />
                <input
                  type="hidden"
                  name="cancel_url"
                  value="http://www.sparkborsa.com/userDashboard"
                />
                <input
                  id="pay-image"
                  type="image"
                  src="https://www.coinpayments.net/images/pub/buynow-wide-blue.png"
                  alt="Buy Now with CoinPayments.net"
                />
              </form>
            )}
          </div>
        </div>
        <Footer className="footer" />
      </div>
    </Wrapper>
  );
};

/* Some color info
bgcolor: #151A22
uppercolor: #2B2F39
fontcolor: #fffefb
yellowcolor: #ff9b02
bluecolor: #526acb
*/

const Wrapper = styled.div`
  position: relative;
  background-color: #151a22;
  .main {
    min-height: 100vh;
    position: relative;
    .top-container {
      padding: 30px 15px;
      .heading {
        display: flex;
        color: white;
        justify-content: space-between;
      }
      form {
        display: flex;
        flex-direction: column;
        row-gap: 1.5rem;
        padding-top: 3rem;
        width: 100%;
        max-width: 500px;
        margin: 2rem auto;
        .pay-btn {
          margin: 0 auto;
        }
        label {
          display: grid;
          grid-template-columns: 1fr 1fr;
          column-gap: 1rem;
          row-gap: 0.5rem;
          text-align: left;
          align-items: center;
          p {
            margin: 0;
            text-align: center;
            font-weight: 500;
            font-size: 1.1rem;
            color: white;
            border-radius: 10px;
            background-color: #2b2f39;
            padding: 5px 0;
          }
          .value {
            background-color: #526acb;
          }
        }
      }
      #pay-image {
        width: 300px;
        height: auto;
        margin: 0 auto 4rem auto;
      }
    }
  }
  .card {
    background-color: #2b2f39;
    border-radius: 25px;
  }
  .btn {
    width: 200px;
    background-color: #2b2f39;
    border: none;
    border-radius: 10px;
    color: #fffefb;
    padding: 10px 0;
    font-weight: 500;
  }

  @media screen and (min-width: 804px) {
    .main {
      margin-left: 120px;
    }
  }

  @media screen and (min-width: 992px) {
    * {
      max-width: 1920px;
    }
  }
`;

export default PayFees;
