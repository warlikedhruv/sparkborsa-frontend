import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Link, useHistory } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowCircleLeft } from "@fortawesome/free-solid-svg-icons";
import { Puff } from "react-loading-icons";
import { useMainContext } from "../context/mainContext";

const Agreement = () => {
  let history = useHistory();
  return (
    <Wrapper>
      <div className="back-btn-container">
        <div onClick={history.goBack}>
          <FontAwesomeIcon className="icon" icon={faArrowCircleLeft} />
        </div>
      </div>
      <h2>User Agreement</h2>
      <h4>
        By creating an account on this site you are agreeing to it's terms and
        conditions
      </h4>
      <div className="card">
        <p>
          {`
Terms and condition
1. Objectives
SPARKBORSA that powered by XLab Research, to help users enter and navigate the cryptocurrency space, and provide tools to help users make their own sound decisions relative to growing their wealth. Services are intended for short, medium- and long-term investors.

2. Activities
2.1 Account creation

SPARKBORSA will create the necessary crypto exchange accounts on behalf of, and with permission of the user. Once account set-up is complete, users are provided with the account credentials and password change instructions, at which point users will have full control over their account. Users are the full and sole owner of their exchange accounts and carry all responsibilities that come with opening, holding, and managing such accounts.

2.2 Asset allocation

SPARKBORSA uses dynamic, proprietary algorithms to define and perform portfolio allocation, based on user input. Manual trades or transactions conducted by users may limit SPARKBORSA account monitoring and reporting ability.

2.3 Fund deposits and withdrawals

Users are responsible for depositing and withdrawing funds to/from their account, however SPARKBORSA will provide clear instructions on how to do so.

2.4 Account monitoring and reporting

SPARKBORSA will continuously monitor user accounts and provide users with a monthly dashboard with easy-to-understand portfolio and performance details.

3. Fees
3.1 Fee structure

SPARKBORSA offers different plans:, performance fee for now.
If you choose the performance fee plan, you pay a our fee of 28% via USDT after we reach higher than 11.99% , after which we won't charge you anything until you hit 11.99% profit. The moment that happens SPARKBORSA will charge you a 28% performance fee over the realized gains, and the baseline is set to the portfolio value. For example: you invest USD 5,000 and after a while your portfolio is worth USD 5,600 (12% profit). You will receive an invoice for the amount of USD 168 (600 profit * 28%), and the baseline is set to USD 5,600. To hit higher than 11.99% again, your portfolio needs to reach USD 6,050 (6,272 + 10%).

Our customers especially like the no-profit-no-pay model as it means that SPARKBORSA only gets paid if the customer wins. We like this also!

3.2 Promotions

SPARKBORSA reserves the right to apply promotional discounts to the fees listed under 3.1. This is completely at SPARKBORSA discretion.

3.3 Other fees

SPARKBORSA does not charge any additional or hidden fees. For any transaction or trading fees that may be charged by cryptocurrency exchanges and/or banks, users accept full responsibility.

4. Security
4.1 Infrastructure

SPARKBORSA is powered by DIGitalocean. DIGitalocean helps reducing the cost, complexity, and risk associated with security and compliance in the cloud.

4.2 Exchanges

SPARKBORSA thoroughly vets its partners and only works with major “top-10” exchanges. User funds remain on their personal exchange account and interactions between SPARKBORSA and user accounts are performed by means of customer owned API keys. API access is limited, hence in the unlikely event of a security breach, no fund withdrawal can take place.


5. Disclaimer
5.1 Financial advice

SPARKBORSA does not provide financial advice. SPARKBORSA is not a Registered Investment Advisor, Broker/Dealer, Financial Analyst, Bank, Securities Broker, Commodity Trade Advisor, or Financial Planner. The information provided, such as optimal fund allocation, is based on user input and for information purposes only. The Information is not intended to be and does not constitute financial advice or any other advice, is general in nature and not specific to you. Before using SPARKBORSA, users should seek the advice of a qualified investment professional and undertake their own due diligence. SPARKBORSA is not responsible for any investment decision made by users. remember we are not a finance company we are IT company



5.2 Warranties

SPARKBORSA disclaims to the full extent authorized by law any and all warranties, whether express or implied, including, without limitation, any implied warranties, merchantability or fitness for a particular purpose. SPARKBORSA does not warrant that its services and applications will be free of bugs, errors, viruses or other defects, nor that trade results will be profitable.

5.3 Liability

In no event can SPARKBORSA or any of its owners be liable to users or any party related to users for any indirect, incidental, consequential, special, exemplary, or punitive damages, lost profits, or losses, even if SPARKBORSA has been advised of the possibility of such damages. In any event, SPARKBORSA’s total aggregate liability to users for all damages of every kind and type (regardless of whether based in contract or tort) shall not exceed the fees paid to SPARKBORSA for the use of our services or applications.

6. Risks
Investing in any asset class, including crypto currencies, holds numerous risks, for example: technology risks, exchange risks, security risks, regulatory risks.  Before deciding to leverage SPARKBORSA services, users should carefully consider their investment objectives and risk appetite. The possibility exists that users lose some or all of their initial investment and therefore no money should be invested that users cannot afford to lose.

As with any market, for a transaction to happen it requires a seller, a buyer, and a price agreement. Loss-limiting strategies such as stop-loss orders may not be effective because market conditions or technological issues may make it impossible to execute stop-loss orders. SPARKBORSA is not responsible for any losses incurred as a result of using SPARKBORSA’s advisory services.


7. Privacy
SPARKBORSA is fully compliant with GDPR (General Data Protection Regulation), an EU regulation enforceable as of May 25, 2018. SPARKBORSA stores the minimum possible amount of user data necessary to deliver its services. Stored user data includes: name, date of birth, country of residence, email address, and exchange API keys (encrypted). User data will never be shared with any third party before explicit user consent. Users have the right for their user data to be forgotten, which can be requested over email.

8. Communication
The User understands and agrees to the preceding sections. The User also agrees to the following:

User agrees to not hold SPARKBORSA liable for any realized trading loss in any currency or asset;
User reserves the right to stop using SPARKBORSA at any point;
You agrees and warrants that any information provided to SPARKBORSA is accurate and complete;
User agrees that SPARKBORSA is offering a Software as a Service (SaaS). This means that SPARKBORSA monitors user accounts on users’ behalf on exchanges that are not affiliated with SPARKBORSA;
User agrees to take full responsibility for identifying and paying any tax liabilities that may exist as a result of trading, or SPARKBORSA services in general;
This agreement is governed by and construed in accordance with the DUBAI law and any dispute, which cannot be amicably resolved, shall be submitted to the jurisdiction of the Court in the UAE;
SPARKBORSA reserves the right to terminate or deny service to anyone, at any time, for any reason.`}
        </p>
      </div>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  font-family: "Astro";
  min-height: 100vh;
  padding-top: 10%;
  padding: 3%;
  background: rgba(43, 51, 54, 0.9);
  /* background-image: url("https://images.unsplash.com/photo-1601662528567-526cd06f6582?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cGxhaW4lMjB3aGl0ZXxlbnwwfHwwfHw%3D&w=1000&q=80");
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  background-blend-mode: soft-light; */
  h2 {
    color: white;
    text-align: center;
    letter-spacing: 2px;
    margin: 0;
  }
  h4 {
    text-align: center;
    color: orange;
    font-size: 1rem;
  }
  .card {
    max-width: 900px;
    margin: 0 auto;
  }
  p {
    color: black;
    margin: 0;
    padding: 1rem;
    font-weight: 500;
    font-size: 1rem;
    white-space: pre-wrap;
  }
  .btn {
    font-weight: 500;
  }
  .back-btn-container {
    display: grid;
    place-items: center;
    font-size: 3rem;
  }
  @media screen and (min-width: 804px) {
    > div {
      max-width: 1920px;
      padding: 0;
      margin: 0 auto;
    }
    input {
      font-size: 1rem;
      min-width: 400px;
    }
  }

  @media screen and (min-width: 992px) {
    *:first-child {
      max-width: 1920px;
      margin: 0 auto;
    }
  }
`;

export default Agreement;
