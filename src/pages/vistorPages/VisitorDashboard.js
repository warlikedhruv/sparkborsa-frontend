import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import axios from "axios";
import { Sidebar, Footer } from "../../components";
import { useMainContext } from "../../context/mainContext";

const APIURL = process.env.REACT_APP_API_URL;

const VisitorDashboard = () => {
  const [status, setStatus] = useState({ type: "warning", text: "" });
  const [showButton, setShowButton] = useState(false);

  const { currentUser } = useMainContext();
  //console.log(currentUser);

  useEffect(() => {
    axios
      .get(APIURL + "/api/fetchUserRequest", {
        params: { username: currentUser.username },
      })
      .then((response) => {
        if (response.data.status === "applied") {
          setStatus({
            type: "success",
            text: "We have received your account request and it is still processing. You will be notified here if your request is rejected. If your request is approved then your account will be automatically elevated.",
          });
        } else if (response.data.status === "rejected") {
          setStatus({
            type: "warning",
            text: "Your account request has been rejected.",
          });
        }
      })
      .catch((error) => {
        //console.log(error);
      });
  }, []);

  return (
    <Wrapper>
      <Sidebar />
      <div className="main">
        <div className="top-container">
          <div className="card display-container">
            <div className="text-data">
              <h3>Welcome to Sparkborsa</h3>
              <p>Improve Your Balance With Our Smart Tools</p>
            </div>
            <div className="img-container moveUpDown">
              <img
                src="https://i.ibb.co/PgkxPpc/s-1.png"
                alt="Bitcoin flowchart"
              />
            </div>
          </div>
          <div className="btn-container">
            <Link to="/addAccount">
              <button className="btn">Add Account</button>
            </Link>
          </div>
        </div>
        <div className="alert-container">
          {status?.text && (
            <div
              className={
                status?.type === "warning"
                  ? "alert alert-danger"
                  : "alert alert-success"
              }
              role="alert"
            >
              {status?.text}
            </div>
          )}
        </div>
        <Footer className="footer" />
      </div>
    </Wrapper>
  );
};

/* Some color info
bgcolor: #151A22
uppercolor: #2B2F39
fontcolor: #fffefb
yellowcolor: #ff9b02
bluecolor: #526acb
*/

const Wrapper = styled.div`
  position: relative;
  background-color: #151a22;
  .main {
    min-height: 100vh;
    position: relative;
    .top-container {
      padding: 30px 15px;
      .display-container {
        background: #2b2f39;
        height: 200px;
        position: relative;
        margin-bottom: 30px;
        .text-data {
          text-align: center;
          padding-top: 2rem;
          h3 {
            font-family: "Moonhouse";
            color: white;
            font-size: 1.5rem;
            margin-bottom: 2rem;
          }
          p {
            color: white;
            font-weight: 500;
            font-size: 1.2rem;
            max-width: 300px;
            margin: 0 auto;
          }
        }
        .img-container {
          display: none;
        }
      }
      .btn-container {
        display: flex;
        justify-content: center;
      }
    }
    .alert-container {
      padding: 20px 50px;
      margin-bottom: 2rem;
    }
  }
  .card {
    background-color: #2b2f39;
    border-radius: 25px;
  }
  .btn {
    width: 200px;
    background-color: #2b2f39;
    border: none;
    border-radius: 10px;
    color: #fffefb;
    padding: 15px 0;
    font-weight: 500;
  }

  @media screen and (min-width: 804px) {
    .main {
      margin-left: 120px;
      .top-container {
        padding: 75px 50px;
        .display-container {
          height: 300px;
          margin-bottom: 80px;
          .text-data {
            max-width: 40%;
            position: absolute;
            bottom: 30px;
            left: 5%;
          }
          .img-container {
            display: flex;
            max-width: 50%;
            position: absolute;
            right: 5%;
            top: -50px;
            img,
            svg {
              max-width: 100%;
              max-height: 400px;
            }
          }
        }
        .btn-container {
          display: flex;
          float: right;
        }
      }
      .accounts-container {
        padding: 0px 50px 75px 50px;
        display: grid;
        gap: 1rem;
        .account-card {
          display: flex;
          flex-direction: row;
          gap: 1rem;
          justify-content: space-between;
          text-align: center;
          align-items: center;
          color: white;
          padding: 25px 50px;
          p {
            margin: 0;
            font-weight: 500;
          }
          a {
            text-decoration: none;
            p {
              color: green;
            }
          }
        }
      }
    }
    .card {
      background-color: #2b2f39;
      border-radius: 25px;
    }
    .btn {
      width: 200px;
      background-color: #2b2f39;
      border: none;
      border-radius: 10px;
      color: #fffefb;
      padding: 15px 0;
      font-weight: 500;
    }
  }

  @media screen and (min-width: 992px) {
    * {
      max-width: 1920px;
    }
  }
`;

export default VisitorDashboard;
