import Dashboard from "./adminPages/Dashboard";
import Login from "./Login";
import Landing from "./Landing";
import Signup from "./Signup";
import SingleUser from "./adminPages/SingleUser";
import UserDashboard from "./userPages/UserDashboard";
import NewUsers from "./adminPages/NewUsers";
import DisplayUser from "./userPages/DisplayUser";
import PayFees from "./userPages/PayFees";
import VisitorDashboard from "./vistorPages/VisitorDashboard";
import AddAccount from "./vistorPages/AddAccount";
import AddAnotherAccount from "./userPages/AddAnotherAccount";
import Agreement from "./Agreement";
import Bots from "./Bots";

export {
  Dashboard,
  UserDashboard,
  PayFees,
  Login,
  Landing,
  Signup,
  SingleUser,
  NewUsers,
  DisplayUser,
  VisitorDashboard,
  AddAccount,
  AddAnotherAccount,
  Agreement,
  Bots
};
