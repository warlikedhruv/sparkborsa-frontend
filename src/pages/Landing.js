import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { LandingNavbar, Footer } from "../components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEye,
  faBars,
  faCamera,
  faLink,
  faStar,
  faArrow,
} from "@fortawesome/free-solid-svg-icons";
import binancelogo from "../assets/images/binance-logo.svg";
import gateiologo from "../assets/images/gateio-logo.svg";
import kucoinlogo from "../assets/images/kucoin-logo.svg";
import okexlogo from "../assets/images/okex-logo.svg";

const Landing = () => {
  const roadmapList = [
    { done: true, item: "Launch Base Sparkborsa Website" },
    { done: true, item: "Launch Spark Engine" },
    { done: true, item: "Binance Spot BETA" },
    { done: true, item: "Bot and Indicators Section" },
    { done: true, item: "Full UI Flexible" },
    { done: true, item: "Binance us Spot BETA" },
    { done: false, item: "Copy-trading section binance spot beta launch" },
    { done: false, item: "copy-trading section" },
    { done: false, item: "Kucoin spot BETA" },
    { done: false, item: "ZETA app signal lunch on ios/android" },
    { done: false, item: "Screens and terminal" },
    { done: false, item: "ZETA tracker launch on ios/android" },
    { done: false, item: "Binance Future Launch" },
    { done: false, item: "Affiliate Program" },
    { done: false, item: "Academy Platform Launch" },
    { done: false, item: "Gate.io Launch" },
    { done: false, item: "Copy-trading Kucoin Launch" },
    { done: false, item: "Broker-Neutral trading platform for analysis" },
    { done: false, item: "Token #1 Preparation" },
    { done: false, item: "Marketplace Release" },
    { done: false, item: "CoinmarketCAp/coingecko" },
    { done: false, item: "Exchange Launch" },
    { done: false, item: "Token #2 Preparation" },
    { done: false, item: "We're Just Starting!" },
  ];

  const data = [
    {
      name: "Jay Spark",
      image: "https://gcdnb.pbrd.co/images/iYACAaCmqw58.jpg",
      designation: "CEO-FOUNDER",
      description: [
        "Yasir (jay spark) is An artificial Intelligence/Data science Programmer , Cryptocurrency trader since 2017(made over 1.2M).",
        "The Founder, COO of XLab Research .Partner (owns 30%) in XLABS AI. Additionally, he Took over Init Factory for smart chips.",
        "He built a crypto data aggregator with loads of helpful tools like SparkBorsa and ZETA companies.He is funding AI programming research for breast cancer screening & identifying , Along with early stage investing in start-up companies in blockchain-based applications.",
      ],
      instagram: " ",
      linkedin: " ",
      color: "Red",
    },
    {
      name: "Turki",
      image: "https://gcdnb.pbrd.co/images/dpA4Rh5THcMU.jpg",
      designation: "CEO",
      description: [
        "Turki had co founded multiple ventures in Energy sectors, Fund raised $50M in Brownfield and Greenfield investments.",
        "Managed international JV for more than 2 decades in Oil and Gas services company of which 2 firms among fortune 500 company. Graduate year 1999 From Solihull College UK, Information Technology.",
      ],
      instagram: " ",
      linkedin: " ",
      color: "Purple",
    },
    {
      name: "Dhruv Patel",
      image: "https://gcdnb.pbrd.co/images/kf0mQASvooWV.jpg",
      designation: "PROJECT LEADER",
      description: [
        "Dhruv Patel is the project lead and handles the architecture of the applications. WIth experience in web development and cloud architecture provider he has developed and deployed various applications while working as a freelance developer.",
      ],
      instagram: "",
      linkedin: "",
      color: "Blue",
    },
    {
      name: "Daksh Barad",
      image: "https://gcdnb.pbrd.co/images/4t8uTPg1nYo0.jpg",
      designation: "DEVELOPER",
      description: [
        "Daksh is a full stack Developer and is responsible for creating and maintaining the sparkborsa website.",
        "He is a freelancer by day and a stock market trader by night. He works on various projects ranging from web development to AI scripts.",
      ],
      instagram: "https://www.instagram.com/daksh_barad/?hl=en",
      linkedin:
        "https://www.linkedin.com/in/daksh-barad-684472189/?originalSubdomain=in",
      color: "Green",
    },
  ];

  return (
    <>
      <LandingNavbar />
      <Wrapper>
        <section id="intro">
          <div className="overlay">
            <img
              src="https://i.ibb.co/RHkDwWD/Pngtree-futuristic-style-black-abstract-dots-5431487.png"
              alt="dots"
              className="clip"
            />
          </div>
          <h2 className="intro-heading">GAIN.</h2>
          <h2 className="intro-heading">DISCIPLINE.</h2>
          <h2 className="intro-heading">PERFORMANCE.</h2>
          <p className="intro-para">
            FIRST QUANTITATIVE DECENTRALIZED HEDGE FUNDS VIA API AND SCANNERS
            BUILD WITH ARTIFICIAL INTELLIGENCE AND MACHINE LEARNING
          </p>
          <div className="nav-btn">
            <Link to="/signup">
              <button className="btn2">JOIN SPARK-ENGINE</button>
            </Link>
          </div>
        </section>
        <section id="spark-engine">
          <div className="heading">
            <h3>SPARK-ENGINE</h3>
            <h4>Join our Smart Trading Engine via API</h4>
          </div>
          <div className="data">
            <article className="data-box">
              <img src="https://i.ibb.co/d773D9x/1.png" alt="" />;
              <h4>Risk Management</h4>
              <p>
                Financial management was built on strong mathematical
                foundations.
              </p>
            </article>
            <article className="data-box">
              <img src="https://i.ibb.co/yQ7d6DK/2.png" alt="" />
              <h4>Security</h4>
              <p>
                We are hashing and encryption your API keys in our servers with
                high level protection.
              </p>
            </article>
            <article className="data-box">
              <img src="https://i.ibb.co/zJ3PHmD/3.png" alt="" />
              <h4>Profit</h4>
              <p>
                Enjoy the passive income profit with high secure from drowdown.
              </p>
            </article>
            <article className="data-box">
              <img src="https://i.ibb.co/RCNxcxD/4.png" alt="" />

              <h4>Millisecond Speed</h4>
              <p>Speed less than 0.01 millisecond to replicate a trade.</p>
            </article>
            <article className="data-box">
              <img src="https://i.ibb.co/4T9vDk7/5.png" alt="" />
              <h4>No Need !!</h4>
              <p>
                No need to send us money to trade or invest for you all in your
                account.
              </p>
            </article>
            <article className="data-box">
              <img src="https://i.ibb.co/8K1SjvL/6.png" alt="" />

              <h4>Risk Management</h4>
              <p>
                Financial management was built on strong mathematical
                foundations.
              </p>
            </article>
          </div>
        </section>
        <section id="bots">
          <h3 className="heading">BOTS AND INDICATORS</h3>
          <p>
            All Bots and Indicators built by Sparkborsa that will help in giving
            the trader the best experience in trading and data analysis using
            Artificial Intelligence Algorithms.
          </p>
          <Link to="/bots">
            <button>VIEW</button>
          </Link>
        </section>
        <section id="about-us">
          <div className="heading">
            <h3>ABOUT US</h3>
          </div>
          <div className="data-container">
            <div className="cards">
              <article className="point-card">
                <p>Multiple Ideas</p>
                <div className="percent">
                  <img
                    src="https://i.ibb.co/SdjdnvR/100.png"
                    alt="100 percent"
                  />
                  <p>100%</p>
                </div>
              </article>
              <article className="point-card">
                <p>Multiple Profit Source</p>
                <div className="percent">
                  <img
                    src="https://i.ibb.co/SdjdnvR/100.png"
                    alt="100 percent"
                  />
                  <p>100%</p>
                </div>
              </article>
              <article className="point-card">
                <p>Multiple High Protection</p>
                <div className="percent">
                  <img
                    src="https://i.ibb.co/SdjdnvR/100.png"
                    alt="100 percent"
                  />
                  <p>100%</p>
                </div>
              </article>
            </div>
            <div className="para">
              <p>
                Our company uses Artificial Intelligence and Deep Learning to
                build our investment tools, A high-level team of programmers,
                and a trading system that checks general and deep liquidity in
                the market, and fake orders are revealed to whales in the
                scanner, checking the blockchain, buying and selling signals,
                and also displaying data in a professional way through separate
                applications
              </p>
            </div>
          </div>
        </section>
        <section id="roadmap">
          <div className="wrapper">
            <h1>Sparkborsa's Roadmap</h1>
            <ul className="sessions">
              {roadmapList.map((item, index) => {
                console.log(item);
                return (
                  <li key={index}>
                    <div className={item.done ? "task done" : "task"}>
                      {item.item}
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
        </section>
        <section id="supported-exchanges">
          <h3>Supported Exchanges</h3>
          <div className="exchanges-list">
            <article className="exchange">
              <img
                className="exchange-logo"
                src={binancelogo}
                alt="binance logo"
              />
              <h3 className="exchange-name">Binance</h3>
              <p className="available">Available</p>
            </article>
            <article className="exchange">
              <img
                className="exchange-logo"
                src={binancelogo}
                alt="binance.us logo"
              />
              <h3 className="exchange-name">Binance.us</h3>
              <p className="available">Available</p>
            </article>
            <article className="exchange">
              <img
                className="exchange-logo"
                src={kucoinlogo}
                alt="kucoin logo"
              />
              <h3 className="exchange-name">Kucoin</h3>
              <p>Coming Soon</p>
            </article>
            <article className="exchange">
              <img className="exchange-logo" src={okexlogo} alt="okex logo" />
              <h3 className="exchange-name">Okex</h3>
              <p>Coming Soon</p>
            </article>
            <article className="exchange">
              <img
                className="exchange-logo"
                src={gateiologo}
                alt="gateio logo"
              />
              <h3 className="exchange-name">Gate.io</h3>
              <p>Coming Soon</p>
            </article>
          </div>
        </section>
        <section id="our-team">
          <header>
            <h2>Team</h2>
          </header>
          <section className="cards-container">
            {data.map((item) => {
              return (
                <article className="material-card mc-active Red">
                  <h2>
                    <span>{item.name}</span>
                    <strong>
                      <FontAwesomeIcon icon={faStar} />
                      {item.designation}
                    </strong>
                  </h2>
                  <div className="img-container">
                    <img src={item.image} alt="" />
                  </div>
                  <div className="description">
                    {item.description.map((line, ind) => {
                      return <p>{line}</p>;
                    })}
                  </div>
                  <div className="socials">
                    <h4>Socials</h4>
                    <a href={item.instagram} target="_blank" rel="noreferrer">
                      <FontAwesomeIcon icon={faCamera} />
                    </a>
                    <a href={item.linkedin} target="_blank" rel="noreferrer">
                      <FontAwesomeIcon icon={faLink} />
                    </a>
                  </div>
                  {/* <button
                    className="action-btn"
                    onClick={(e) => {
                      console.log(e.target.tagName);
                      var card = e.target.parentElement;
                      var icon = e.target;
                      if (e.target.tagName === "path") {
                        card =
                          e.target.parentElement.parentElement.parentElement;
                        icon = e.target.parentElement.parentElement;
                        console.log("This");
                      }
                      console.log(card, icon);
                      icon.classList.add("fa-spin-fast");

                      if (card.classList.contains("mc-active")) {
                        card.classList.remove("mc-active");
                        window.setTimeout(() => {
                          icon.classList.remove("fa-arrow-left");
                          icon.classList.remove("fa-spin-fast");
                          icon.classList.add("fa-bars");
                        }, 800);
                      } else {
                        card.classList.add("mc-active");
                        window.setTimeout(() => {
                          icon.classList.remove("fa-bars");
                          icon.classList.remove("fa-spin-fast");
                          icon.classList.add("fa-arrow-left");
                        }, 800);
                      }
                    }}
                  >
                    <FontAwesomeIcon icon={faStar} />
                  </button> */}
                </article>
              );
            })}
          </section>
        </section>
      </Wrapper>
    </>
  );
};

const Wrapper = styled.div`
  $colors: (
    "red": #f44336,
    "purple": #673ab7,
    "blue": #3f51b5,
    "green": #009688,
  );
  font-family: "Astro";
  background-color: #2b3336;
  #intro,
  #spark-engine,
  #about-us,
  #our-team {
    padding: 1rem 0;
  }
  #intro {
    max-width: 100%;
    position: relative;
    min-height: 100vh;
    padding-top: 100px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: 0 auto;
    padding-left: 10px;
    .overlay {
      width: 70%;
      height: 100%;
      position: absolute;
      top: 0;
      right: 0;
      overflow-x: hidden;
      overflow-y: hidden;
      .clip {
        height: 100%;
        position: absolute;
        left: 0%;
      }
    }
    .intro-heading {
      font-size: 2rem;
      max-width: 80%;
      color: white;
      margin: 0;
      z-index: 1;
    }
    .intro-para {
      font-size: 0.75rem;
      color: white;
      font-weight: 100;
      max-width: 50%;
      margin: 10px 0 0 0;
      z-index: 1;
    }
    .nav-btn {
      z-index: 1;
      .btn2 {
        color: #2b3336;
        background: linear-gradient(to right, #6bff8e, #00919e);
        border: none;
        padding: 0.25rem 0.5rem;
        border-radius: 10px;
        margin: 15px 0 0 0;
      }
    }
  }

  /* ---------------Styling for spark-engine------------ */
  #spark-engine {
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: 95%;
    margin: 0 auto;
    color: white;
    .heading {
      text-align: center;
      margin-bottom: 3rem;
    }
    .data {
      display: grid;
      grid-template-columns: 1fr;
      gap: 1rem;
      .data-box {
        h4 {
          margin-top: 15px;
        }
        p {
          color: white;
          font-size: 0.8rem;
        }
        .icon {
          font-size: 2rem;
          color: #6bff8e;
        }
      }
    }
  }

  /* ---------------Styling for bots------------ */
  #bots {
    /* min-height: 100vh; */
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: 95%;
    margin: 0 auto;
    color: white;
    .heading {
      text-align: center;
      margin-bottom: 3rem;
    }
    p {
      max-width: 350px;
      text-align: center;
      margin: 0 auto 3rem auto;
      color: white;
    }
    a {
      margin: 0 auto;
    }
    button {
      color: #2b3336;
      background: linear-gradient(to right, #6bff8e, #00919e);
      border: none;
      padding: 0.25rem 1.5rem;
      border-radius: 10px;
      margin: 15px auto 0 auto;
      width: fit-content;
    }
  }

  /* ---------------Styling for bots------------ */
  #roadmap {
    height: 100vh;
    margin: 0 auto;
    color: black;
    background: #2b3336;
    display: grid;
    place-items: center;
    ul,
    li {
      list-style: none;
      padding: 0;
    }

    .wrapper {
      background: #eaf6ff;
      padding: 2rem;
      border-radius: 15px;
      width: 90%;
      margin: 0 auto;
      height: 90%;
      overflow: scroll;
    }
    .wrapper::-webkit-scrollbar {
      display: none;
    }

    h1 {
      font-size: 1.25rem;
      font-weight: 600;
      font-family: sans-serif;
    }

    .sessions {
      margin-top: 2rem;
      border-radius: 12px;
      display: flex;
      flex-direction: column;
    }

    li {
      padding-bottom: 1.5rem;
      border-left: 1px solid #abaaed;
      position: relative;
      padding-left: 20px;
      margin-left: 10px;
    }
    li:last-child {
      border: 0px;
      padding-bottom: 0;
    }
    li:before {
      content: "";
      width: 15px;
      height: 15px;
      background: white;
      border: 1px solid #4e5ed3;
      box-shadow: 3px 3px 0px #bab5f8;
      box-shadow: 3px 3px 0px #bab5f8;
      border-radius: 50%;
      position: absolute;
      left: -10px;
      top: 0px;
    }

    .done:before {
      content: "";
      width: 15px;
      height: 15px;
      background: lightgreen;
      border: 1px solid #4e5ed3;
      box-shadow: 3px 3px 0px #bab5f8;
      box-shadow: 3px 3px 0px #bab5f8;
      border-radius: 50%;
      position: absolute;
      left: -10px;
      top: 0px;
    }

    .task {
      color: #2a2839;
      font-family: "Poppins", sans-serif;
      font-weight: 500;
    }
    @media screen and (min-width: 601px) {
      .task {
        font-size: 0.9rem;
      }
    }
    @media screen and (max-width: 600px) {
      .task {
        margin-bottom: 0.3rem;
        font-size: 0.85rem;
      }
    }

    p {
      color: #4f4f4f;
      font-family: sans-serif;
      line-height: 1.5;
      margin-top: 0.4rem;
    }
    @media screen and (max-width: 600px) {
      p {
        font-size: 0.9rem;
      }
    }
  }

  /* --------------Styling for contact------------------ */
  #about-us {
    /* min-height: 100vh; */
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: 95%;
    margin: 0 auto;
    color: white;
    .heading {
      text-align: center;
      margin-bottom: 3rem;
    }
    .data-container {
      display: grid;
      place-items: center;
      gap: 3rem;
      .cards {
        display: flex;
        flex-direction: column;
        gap: 1rem;
        justify-content: center;
        .point-card {
          display: flex;
          justify-content: space-between;
          align-items: center;
          gap: 1rem;
          p {
            font-size: 1.2rem;
            margin: 0;
            color: white;
          }
          .percent {
            position: relative;
            img {
              width: 75px;
            }
            p {
              font-size: 0.75rem;
              position: absolute;
              top: 40%;
              left: 30%;
            }
          }
        }
      }
      .para {
        max-width: 600px;
        p {
          margin: 0;
          font-size: 1rem;
          text-transform: uppercase;
          text-align: center;
          color: white;
        }
      }
    }
  }

  /* --------------Styling for Supported Exchanges */
  #supported-exchanges {
    display: flex;
    flex-direction: column;
    justify-content: center;
    width: 95%;
    margin: 0 auto;
    color: black;
    margin-top: 5rem;
    h3 {
      color: white;
      text-align: center;
    }
    .exchanges-list {
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
      align-items: center;
      .exchange {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        background-color: white;
        margin: 1rem;
        width: 150px;
        padding: 1rem;
        border-radius: 15px;
        h3 {
          margin-top: 10px;
          margin-bottom: 0;
          font-size: 1rem;
          color: black;
        }
        p {
          color: red;
          font-size: 0.75rem;
          margin: 0;
        }
        p.available {
          color: green;
        }
        img {
          width: 100px;
        }
      }
    }
  }

  #our-team {
    width: 95%;
    margin: 3rem auto;
    color: white;
    // Animations
    .fa-spin-fast {
      animation: fa-spin-fast 0.2s infinite linear;
    }

    @keyframes fa-spin-fast {
      0% {
        transform: rotate(0deg);
      }
      100% {
        transform: rotate(359deg);
      }
    }

    header h2 {
      text-align: center;
      padding: 1rem 1rem;
      margin: 0;
      margin-bottom: 2rem;
      font-size: 2rem;
    }
    .cards-container {
      display: flex;
      flex-wrap: wrap;
      justify-content: space-around;
      gap: 2rem;
      .material-card {
        background-color: black;
        border-radius: 15px;
        width: 250px;
        height: 400px;
        position: relative;
        overflow: hidden;
        h2 {
          height: 80px;
          width: 250px;
          margin: 0;
          padding: 10px 20px;
          position: absolute;
          top: 320px;
          display: flex;
          flex-direction: column;
          justify-content: center;
          transition: all 0.5s;
          span {
            display: block;
            width: fit-content;
            font-size: 1.3rem;
          }
          strong {
            font-weight: 400;
            display: block;
            width: fit-content;
            font-size: 1rem;
          }
        }
        .img-container {
          position: absolute;
          width: 100%;
          height: 320px;
          transition: all 0.2s;
          z-index: 5;
          img {
            width: 100%;
            height: 100%;
          }
        }
        .action-btn {
          position: absolute;
          width: 40px;
          height: 40px;
          top: 35px;
          right: 30px;
          border-radius: 50%;
          transition: all 0.5s;
          z-index: 10;
          border: 3px solid white;
        }
        .description {
          position: absolute;
          top: 400px;
          width: 210px;
          margin: 10px 20px;
          height: 200px;
          transition: all 0.8s;
          overflow-y: scroll;
          p {
            color: white;
            font-size: 0.9rem;
          }
          ::-webkit-scrollbar {
            display: none;
          }
        }
        .socials {
          position: absolute;
          top: 600px;
          width: 100%;
          height: 100px;
          transition: all 1s;
          padding: 20px;
          color: black;
          background: white;
          h4 {
            width: fit-content;
            margin: 0;
            font-size: 0.9rem;
            font-weight: 700;
            transition: all 1.4s;
            /* background: black; */
          }

          a {
            float: left;
            width: 40px;
            height: 40px;
            margin-left: 10px;
            margin-bottom: 15px;
            font-size: 26px;
            color: #fff;
            line-height: 52px;
            text-decoration: none;
            svg {
              color: black;
            }
          }
        }
        &.mc-active {
          h2 {
            width: 100%;
            top: 0px;
            padding: 10px 10px 10px 90px;
            span {
              font-size: 1rem;
            }
            strong {
              font-size: 0.7rem;
            }
          }
          .img-container {
            top: 0;
            margin: 5px;
            width: 70px;
            height: 70px;
            img {
              border-radius: 50%;
            }
          }
          .action-btn {
            top: 85px;
          }
          .description {
            top: 100px;
          }
          .socials {
            top: 300px;
          }
        }
      }
    }
  }

  @media screen and (min-width: 804px) {
    #intro {
      margin-left: 30px;
      .overlay {
        .clip {
          left: auto;
          right: 0%;
        }
      }
    }
    /* ---------------Styling for spark-engine------------ */
    #spark-engine {
      .data {
        grid-template-columns: 1fr 1fr;
      }
    }

    #our-team {
      .card-container {
        grid-template-columns: 1fr 1fr;
      }
    }
  }

  @media screen and (min-width: 992px) {
    * {
      max-width: 1920px;
    }
    #intro {
      max-width: 1920px;
      margin: 0 auto;
      padding-left: 30px;
      .intro-heading {
        font-size: 3rem;
      }
      .intro-para {
        font-size: 1.2rem;
      }
    }

    /* ---------------Styling for spark-engine------------ */
    #spark-engine {
      min-height: 100vh;
      width: 80%;
      .heading {
        margin-bottom: 3rem;
      }
      .data {
        grid-template-columns: 1fr 1fr 1fr;
      }
    }

    /* --------------Styling for contact------------------ */
    #about-us {
      height: 100vh;
      width: 80%;
      .heading {
        margin-bottom: 3rem;
      }
      .data-container {
        grid-template-columns: 1fr 2fr;
        gap: 1rem;
        .para {
          justify-self: right;
        }
      }
    }

    #roadmap {
      .sessions {
        flex-wrap: wrap;
        max-height: 90%;
      }
    }

    #our-team {
      width: 80%;
      .card-container {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
      }
    }
  }
`;

export default Landing;
