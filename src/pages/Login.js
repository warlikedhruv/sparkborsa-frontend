import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Footer } from "../components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowCircleLeft } from "@fortawesome/free-solid-svg-icons";
import { Puff } from "react-loading-icons";
import { useMainContext } from "../context/mainContext";

const Login = (props) => {
  const { fetchAndSetToken } = useMainContext();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState({ type: "warning", text: "" });

  useEffect(() => {
    if (props?.location?.state) {
      //console.log(props);
      setMessage({ type: "success", text: props?.location?.state?.message });
    }
    return () => {
      setUsername("");
      setPassword("");
      setLoading(false);
      setMessage({ ...message, text: "" });
    };
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    setMessage({ type: "warning", text: "" });
    setLoading(true);

    if (username && password) {
      await fetchAndSetToken(username, password).then(
        () => {
          setMessage({
            type: "success",
            text: "Login Successful",
          });
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          setMessage({ ...message, text: resMessage });
        }
      );
    } else {
      setMessage({ ...message, text: "Username and Password cannot be empty" });
    }
    setLoading(false);
  };

  return (
    <LoginWrapper>
      <div className="main">
        <div className="form-container">
          <div className="logo-container">
            <Link to="/">
              <img
                className="logo"
                src="https://i.ibb.co/dWm9yyY/Rectangle.png"
                alt="logo"
              />
            </Link>
          </div>
          <h1>LOGIN</h1>
          <form onSubmit={handleSubmit}>
            <label>
              <p>Username</p>
              <input
                type="text"
                onChange={(e) => setUsername(e.target.value)}
                placeholder="Enter your username"
              />
            </label>
            <label>
              <p>Password</p>
              <input
                type="password"
                onChange={(e) => setPassword(e.target.value)}
                placeholder="Enter your password"
              />
            </label>
            <div>
              <button className="btn btn-outline-primary" type="submit">
                Submit
              </button>
            </div>
            <p className="signup">
              Don't have an account?{" "}
              <a href="/signup">
                <span>Sign Up</span>
              </a>
            </p>
            {message?.text && (
              <div className="form-group">
                <div
                  className={
                    message?.type === "warning"
                      ? "alert alert-danger"
                      : "alert alert-success"
                  }
                  role="alert"
                >
                  {message?.text}
                </div>
              </div>
            )}
            {loading && (
              <div className="form-group">
                <Puff stroke="#000000" />
              </div>
            )}
          </form>
        </div>
        <div className="image-container">
          <img
            className="moveUpDown"
            src="https://i.ibb.co/jLSkCst/startup-svg-clipart-xl.png"
            alt="loginin sideimage"
          />
        </div>
      </div>
      <Footer />
    </LoginWrapper>
  );
};

const LoginWrapper = styled.div`
  font-family: "Astro";
  min-height: 100vh;
  padding-top: 10%;
  padding: 3%;
  padding-bottom: 0;
  background: #151a22;
  display: grid;
  place-items: center;
  .main {
    display: grid;
    place-items: center;
    position: relative;
    padding-bottom: 5rem;
    .form-container {
      .logo-container {
        width: 100%;
        display: grid;
        place-items: center;
        margin-bottom: 10px;
      }
      h1 {
        color: white;
        text-align: center;
        letter-spacing: 2px;
        margin: 0;
        font-size: 1.5rem;
      }
      p {
        color: white;
        margin: 0;
        text-align: center;
        font-weight: 500;
        font-size: 1rem;
      }
      input {
        border: 2px solid transparent;
        /* background-color: #151a22; */
        border-radius: 10px;
        max-width: 400px;
        width: 100%;
        text-align: center;
        margin: 0 auto;
        padding: 0.25rem 0.5rem;
        font-size: 0.75rem;
        font-family: "poppins";
      }
      form {
        display: flex;
        flex-direction: column;
        align-items: center;
        gap: 1rem;
        margin: 1rem auto;
      }
      .signup {
        a {
          text-decoration: none;
        }
        span {
          color: greenyellow;
        }
      }
      label {
        width: 100%;
        display: grid;
      }
    }
    .image-container {
      img {
        width: 100%;
        max-width: 500px;
      }
    }
  }

  @media screen and (min-width: 804px) {
    > div {
      max-width: 1920px;
      padding: 0;
    }
    input {
      font-size: 1rem;
      min-width: 400px;
    }
    .main {
      grid-template-columns: 1fr 1fr;
      gap: 0;
    }
  }

  @media screen and (min-width: 992px) {
    *:first-child {
      margin: 0 auto;
      max-width: 1920px;
    }
  }
`;

export default Login;
