import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";

const Bots = () => {
  return (
    <Wrapper>
      <div className="heading">
        <h4>Bots And Indicators</h4>
        <Link to="/">
          <FontAwesomeIcon className="back-arrow" icon={faArrowLeft} />
        </Link>
      </div>
      <div className="bots-container">
        <section className="bot">
          <div className="image-container">
            <img
              src="https://gcdnb.pbrd.co/images/rXaWZPhrRpjI.png"
              alt="spark tracker"
            />
          </div>
          <h3 className="bot-name">SPARK TRACKER TA</h3>
          <p className="bot-type">Technical Analysis Bot</p>
          <a
            href="https://t.me/sparktracker"
            target="_blank"
            rel="nonreferrer noreferrer"
          >
            <button>VIEW</button>
          </a>
        </section>
        <section className="bot">
          <div className="image-container">
            <img
              src="https://gcdnb.pbrd.co/images/rolcIdUUtdo6.png"
              alt="spark tracker"
            />
          </div>
          <h3 className="bot-name">SPARK BTC SAFE TREND</h3>
          <p className="bot-type">
            This bot gives you alerts about bitcoin buy/sell
          </p>
          <a
            href="https://t.me/sparkbtcsafe"
            target="_blank"
            rel="nonreferrer noreferrer"
          >
            <button>VIEW</button>
          </a>
        </section>
        <section className="bot coming-soon">
          <h3 className="heading">COMING SOON</h3>
        </section>
        <section className="bot coming-soon">
          <h3 className="heading">COMING SOON</h3>
        </section>
        <section className="bot coming-soon">
          <h3 className="heading">COMING SOON</h3>
        </section>
        <section className="bot coming-soon">
          <h3 className="heading">COMING SOON</h3>
        </section>
      </div>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  font-family: "Astro";
  background-color: #2b3336;
  max-width: 100%;
  position: relative;
  min-height: 100vh;
  padding-top: 50px;
  margin: 0 auto;
  padding-left: 10px;

  .heading {
    display: flex;
    flex-direction: column;
    align-items: center;
    color: white;
    .back-arrow {
      font-size: 1.5rem;
      color: greenyellow;
    }
  }
  .bots-container {
    display: grid;
    gap: 1rem;
    .bot {
      border: 2px solid rgba(173, 255, 43, 0.3);
      border-radius: 20px;
      padding: 0.5rem 1rem;
      display: flex;
      flex-direction: column;
      align-items: center;
      text-align: center;
      .image-container {
        position: relative;
        img {
          /* position: absolute; */
          top: -10%;
          width: 200px;
        }
      }
      button {
        color: #2b3336;
        background: linear-gradient(to right, #6bff8e, #00919e);
        border: none;
        padding: 0.25rem 1.5rem;
        border-radius: 10px;
        margin: 5px auto 0 auto;
        width: fit-content;
      }
      .bot-name {
        color: white;
      }
      .bot-type {
        color: white;
      }
      :hover {
        border-color: rgba(255, 0, 0, 0.3);
      }
    }
    .coming-soon {
      display: grid;
      place-items: center;
    }
  }

  @media screen and (min-width: 804px) {
    .heading {
      margin: 0 2rem;
      flex-direction: row;
      justify-content: space-between;
    }
    .bots-container {
      margin-top: 2rem;
      grid-template-columns: 1fr 1fr 1fr;
    }
  }

  @media screen and (min-width: 992px) {
  }
`;

export default Bots;
