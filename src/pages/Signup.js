import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { Footer } from "../components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowCircleLeft } from "@fortawesome/free-solid-svg-icons";
import { Puff } from "react-loading-icons";
import styled from "styled-components";
import axios from "axios";
import { useMainContext } from "../context/mainContext";

const APIURL = process.env.REACT_APP_API_URL;

const Signup = () => {
  const { fetchAndSetToken } = useMainContext();
  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const [data, setData] = useState({
    username: "",
    name: "",
    email: "",
    password: "",
    confirmPassword: "",
    address: "",
    phone_number: "",
  });

  // function to check the data and call function that sends request
  const handleSubmit = async (e) => {
    e.preventDefault();

    setMessage("");
    setLoading(true);

    if (
      !data.username ||
      !data.name ||
      !data.email ||
      !data.password ||
      !data.confirmPassword ||
      !data.address ||
      !data.phone_number
    ) {
      setMessage("All fields are mandatory!");
    } else if (data.password.length < 8) {
      setMessage("Password should be greater than 7 characters");
    } else if (data.password !== data.confirmPassword) {
      setMessage("Password and confirm password do not match");
    } else {
      // post request to create account
      await axios
        .post(APIURL + "/api/auth/signup", data)
        .then((response) => {
          setMessage(response.data.message);
          setRedirect(true);
        })
        .catch((error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          setMessage(resMessage);
        });
    }
    setLoading(false);
  };

  return (
    <LoginWrapper>
      {redirect && (
        <Redirect
          to={{
            pathname: "/login",
            state: {
              message: "Registration was successful, please login here!",
            },
          }}
        />
      )}
      <div className="main">
        <div className="form-container">
          <div className="logo-container">
            <Link to="/">
              <img
                className="logo"
                src="https://i.ibb.co/dWm9yyY/Rectangle.png"
                alt="logo"
              />
            </Link>
          </div>
          <h1>SIGN IN</h1>
          <h6>
            By creating an account on this site you are agreeing to it's{" "}
            <Link to="/agreement">
              <span>Terms and Condition's</span>
            </Link>
          </h6>
          <form onSubmit={handleSubmit}>
            <label>
              <p>Username</p>
              <input
                type="text"
                onChange={(e) => {
                  let temp = data;
                  temp.username = e.target.value;
                  setData(temp);
                }}
                placeholder="Enter your username"
              />
            </label>
            <label>
              <p>Name</p>
              <input
                type="text"
                onChange={(e) => {
                  let temp = data;
                  temp.name = e.target.value;
                  setData(temp);
                }}
                placeholder="Enter your name"
              />
            </label>
            <label>
              <p>Email</p>
              <input
                type="email"
                onChange={(e) => {
                  let temp = data;
                  temp.email = e.target.value;
                  setData(temp);
                }}
                placeholder="Enter your email"
              />
            </label>
            <label>
              <p>Password</p>
              <input
                type="password"
                onChange={(e) => {
                  let temp = data;
                  temp.password = e.target.value;
                  setData(temp);
                }}
                placeholder="Enter your password"
              />
            </label>
            <label>
              <p>Confirm Password</p>
              <input
                type="password"
                onChange={(e) => {
                  let temp = data;
                  temp.confirmPassword = e.target.value;
                  setData(temp);
                }}
                placeholder="Re-enter your password"
              />
            </label>
            <label>
              <p>Address</p>
              <input
                type="text"
                onChange={(e) => {
                  let temp = data;
                  temp.address = e.target.value;
                  setData(temp);
                }}
                placeholder="Enter your address"
              />
            </label>
            <label>
              <p>Phone Number</p>
              <input
                type="tel"
                onChange={(e) => {
                  let temp = data;
                  temp.phone_number = e.target.value;
                  setData(temp);
                }}
                placeholder="Enter your phone number"
              />
            </label>
            <div>
              <button className="btn btn-outline-primary" type="submit">
                Submit
              </button>
            </div>
            <p className="signup">
              Already have an account?{" "}
              <a href="/login">
                <span>LOG IN</span>
              </a>
            </p>
          </form>
          {message && (
            <div className="message-container">
              <div className="alert alert-danger" role="alert">
                {message}
              </div>
            </div>
          )}
          {loading && (
            <div className="loading-container">
              <Puff stroke="#000000" />
            </div>
          )}
        </div>
        <div className="image-container">
          <img
            className="moveUpDown"
            src="https://i.ibb.co/c2cHmQk/web-hosting-svg-clipart-xl.png"
            alt="loginin sideimage"
          />
        </div>
      <Footer />
      </div>
    </LoginWrapper>
  );
};

const LoginWrapper = styled.div`
  font-family: "Astro";
  min-height: 100vh;
  padding-top: 10%;
  padding: 3%;
  background: #151a22;
  display: grid;
  place-items: center;
  .main {
    display: grid;
    place-items: center;
    position: relative;
    padding-bottom: 5rem;
    .form-container {
      margin-bottom: 1rem;
      .logo-container {
        width: 100%;
        display: grid;
        place-items: center;
        margin-bottom: 10px;
      }
      h1 {
        color: white;
        text-align: center;
        letter-spacing: 2px;
        font-size: 1.5rem;
      }
      p {
        color: white;
        margin: 0;
        text-align: center;
        font-weight: 500;
        font-size: 1rem;
      }
      h6 {
        text-align: center;
        color: grey;
        span {
          color: orange;
        }
      }
      input {
        border: 2px solid transparent;
        border-radius: 10px;
        max-width: 400px;
        width: 100%;
        text-align: center;
        margin: 0 auto;
        padding: 0.25rem 0.5rem;
        font-size: 0.75rem;
        font-family: "poppins";
      }
      form {
        display: grid;
        grid-template-columns: 1fr;
        place-items: center;
        gap: 1rem;
        width: 90%;
        margin: 2rem auto;
      }
      .signup {
        a {
          text-decoration: none;
        }
        span {
          color: greenyellow;
        }
      }
      label {
        width: 100%;
        display: grid;
      }
      .loading-container {
        display: grid;
        place-items: center;
      }
      .message-container {
        width: 60%;
        margin: 0 auto;
        text-align: center;
      }
    }
    .image-container {
      img {
        width: 100%;
        max-width: 500px;
      }
    }
  }

  @media screen and (min-width: 804px) {
    > div {
      max-width: 1920px;
      padding: 0;
    }
    input {
      font-size: 1rem;
      min-width: 400px;
    }
    .main {
      grid-template-columns: 1fr 1fr;
      gap: 0;
    }
  }

  @media screen and (min-width: 992px) {
    *:first-child {
      margin: 0 auto;
      max-width: 1920px;
    }
  }
`;

export default Signup;
