import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Sidebar, Footer } from "../../components";
import { useAdminContext } from "../../context/adminContext";

const APIURL = process.env.REACT_APP_API_URL;

const NewUsers = () => {
  const [setupDone, setSetupDone] = useState(false);
  const { newUsersList, userAccountRequests, socket } = useAdminContext();
  const [message, setMessage] = useState({ type: "warning", text: "" });
  const [show, setShow] = useState(false);

  // Handle approval/disapproval of new user
  const handleNewUser = async (type, userid) => {
    socket.emit(
      "Handle Newuser Request",
      {
        auth: { token: localStorage.getItem("token") },
        newUserId: userid,
        // remove_account: "ab230fdb",
        // status: "remove",
        status: type,
      },
      (data) => {
        //console.log(data);
      }
    );
  };

  // Handle approval/disapproval of user account request
  const handleUserAccountRequest = async (type, userid) => {
    socket.emit(
      "Handle User Account Request",
      {
        auth: { token: localStorage.getItem("token") },
        newUserId: userid,
        status: type,
      },
      (data) => {
        //console.log(data);
      }
    );
  };

  const showMessage = () => {
    setShow(true);
    setTimeout(() => {
      setShow(false);
    }, 3000);
  };

  useEffect(() => {
    if (socket && !setupDone) {
      //console.log("Setting it up");
      socket?.on("User Request Removed", () => {
        //console.log("User request removed");
        setMessage({ type: "warning", text: "User request was rejected." });
        showMessage();
      });
      socket?.on("User Setup Successful", () => {
        //console.log("User Setup Successful");
        setMessage({
          type: "success",
          text: "User request was approved. A new user is created.",
        });
        showMessage();
      });
      socket?.on("User Account Added Successfully", () => {
        //console.log("User Account Added Successfully");
        setMessage({
          type: "success",
          text: "User account request was approved. A new account is added to existing user.",
        });
        showMessage();
      });
      setSetupDone(true);
      // console.log(socket._callbacks);
    }
    return () => {
      socket.removeListener("User Request Removed");
      socket.removeListener("User Setup Successful");
      socket.removeListener("User Account Added Successfully");
    };
  }, []);

  return (
    <Wrapper>
      <Sidebar />
      <div className="main">
        {show && (
          <div
            className={
              message?.type === "warning"
                ? "alert alert-danger"
                : "alert alert-success"
            }
            role="alert"
          >
            {message?.text}
          </div>
        )}
        <h4>Welcome to Sparkborsa</h4>
        <section className="card-container">
          {newUsersList.map((user, index) => {
            return (
              <article
                className={
                  (index % 3 === 0 && "card newuser-card yellow-border") ||
                  (index % 2 === 0 && "card newuser-card red-border") ||
                  "card newuser-card blue-border"
                }
                key={user._id}
              >
                <div className="newuser-item">
                  <h3>Username: </h3>
                  <p>{user.username}</p>
                </div>
                <div className="newuser-item">
                  <h3>Name: </h3>
                  <p>{user.name}</p>
                </div>
                <div className="newuser-item">
                  <h3>Balance: </h3>
                  <p>${parseFloat(user?.application?.balance).toFixed(2)}</p>
                </div>
                <div className="newuser-item">
                  <h3>Strategy: </h3>
                  <p>{user?.application?.strategy}</p>
                </div>
                <div className="newuser-item">
                  <h3>TeleAccount: </h3>
                  <p>{user?.application?.telUsername}</p>
                </div>
                <div className="newuser-item approve-btns">
                  <button
                    className="btn btn-success"
                    onClick={() => {
                      handleNewUser("approve", user._id);
                    }}
                  >
                    Approve
                  </button>
                  <button
                    className="btn btn-danger"
                    onClick={() => {
                      handleNewUser("disapprove", user._id);
                    }}
                  >
                    Disapprove
                  </button>
                </div>
              </article>
            );
          })}
          {userAccountRequests.map((user, index) => {
            return (
              <article
                className={
                  (index % 3 === 0 && "card newuser-card yellow-border") ||
                  (index % 2 === 0 && "card newuser-card red-border") ||
                  "card newuser-card blue-border"
                }
                key={user._id}
              >
                <div className="newuser-item">
                  <h3>Username: </h3>
                  <p>{user.username}</p>
                </div>
                <div className="newuser-item">
                  <h3>Balance: </h3>
                  <p>${parseFloat(user?.application?.balance).toFixed(2)}</p>
                </div>
                <div className="newuser-item">
                  <h3>Strategy: </h3>
                  <p>{user?.application?.strategy}</p>
                </div>
                <div className="newuser-item">
                  <h3>TeleAccount: </h3>
                  <p>{user?.application?.telUsername}</p>
                </div>
                <div className="newuser-item approve-btns">
                  <button
                    className="btn btn-success"
                    onClick={() => {
                      handleUserAccountRequest("approve", user._id);
                    }}
                  >
                    Approve
                  </button>
                  <button
                    className="btn btn-danger"
                    onClick={() => {
                      handleUserAccountRequest("disapprove", user._id);
                    }}
                  >
                    Disapprove
                  </button>
                </div>
              </article>
            );
          })}
          <Footer />
        </section>
      </div>
    </Wrapper>
  );
};

/* Some color info
bgcolor: #151A22
uppercolor: #2B2F39
fontcolor: #fffefb
yellowcolor: #ff9b02
bluecolor: #526acb
*/
const Wrapper = styled.div`
  position: relative;
  background-color: #151a22;
  color: #fffefb;
  .main {
    position: relative;
    min-height: 100vh;
    padding: 2rem 2rem 1rem 2rem;
    h4 {
      text-align: center;
    }
  }
  .card {
    border-radius: 25px;
    background-color: #2b2f39;
    color: white;
  }
  .btn {
    border-radius: 25px;
  }
  .card-container {
    margin-top: 3rem;
    display: flex;
    flex-direction: row;
    gap: 3rem;
    justify-content: space-around;
    flex-wrap: wrap;
    .newuser-card {
      padding: 1rem;
      width: 100%;
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      justify-content: center;
      align-items: center;
      gap: 1rem;
      .newuser-item {
        width: 300px;
      }
      .approve-btns {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        gap: 1rem;
        .btn {
          width: 200px;
        }
      }
      p {
        margin: 0;
        text-align: center;
        color: white;
      }
      h3 {
        text-align: center;
        font-weight: 500;
        font-size: 1.4rem;
      }
    }
    .yellow-border {
      border-left: 4px solid #ff9b02;
      color: #ff9b02;
    }
    .red-border {
      border-left: 4px solid red;
      color: red;
    }
    .blue-border {
      border-left: 4px solid #526acb;
      color: #526acb;
    }
    .value {
      color: whitesmoke;
    }
  }

  @media screen and (min-width: 804px) {
    position: relative;
    background-color: #151a22;
    color: #fffefb;
    .main {
      margin-left: 120px;
      padding: 2rem 2rem 1rem 1rem;
    }
  }

  @media screen and (min-width: 992px) {
    .main {
      max-width: 1920px;
    }
  }
`;

export default NewUsers;
