import React, { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Sidebar, Footer, useCalculateData } from "../../components";
import { useAdminContext } from "../../context/adminContext";
import { createChart } from "lightweight-charts";

let areaSeries;

const Dashboard = () => {
  const { getAllData } = useCalculateData();
  const [graphStatus, setGraphStatus] = useState(false);
  // const {logs} = useAdminContext();
  const {
    totUsers,
    totAccounts,
    totOnlineAccounts,
    totBalance,
    totCashBalance,
    totLockedBalance,
    gData
  } = getAllData();

  // Reference hook to get size of chart div
  const chartRef = useRef();

  useEffect(() => {
    console.log("Am I called");
    var chart = createChart(chartRef.current, {
      width: chartRef?.current?.clientWidth - 15,
      height: chartRef?.current?.clientHeight - 15,
      layout: {
        backgroundColor: "#000000",
        textColor: "#d1d4dc",
      },
      rightPriceScale: {
        scaleMargins: {
          top: 0.3,
          bottom: 0.25,
        },
      },
      crosshair: {
        vertLine: {
          width: 5,
          color: "rgba(224, 227, 235, 0.1)",
          style: 0,
        },
        horzLine: {
          visible: false,
          labelVisible: false,
        },
      },
      timeScale: {
        timeVisible: true,
      },
      grid: {
        vertLines: {
          color: "rgba(42, 46, 57, 0)",
        },
        horzLines: {
          color: "rgba(42, 46, 57, 0)",
        },
      },
    });
    areaSeries = chart.addAreaSeries({
      lineWidth: 2,
      crossHairMarkerVisible: false,
    });
    
    // Setting resize observer for chart Containers, lets test again, agin
    const ro = new ResizeObserver(() => {
      const elemWidth = chartRef?.current?.clientWidth;
      const elemHeight = chartRef?.current?.clientHeight;
      if (elemWidth && elemHeight) {
        chart.resize(elemWidth - 15, elemHeight - 15);
      }
    });
    ro.observe(chartRef.current);
    return () => {
      ro.disconnect();
    };
  }, []);

  useEffect(() => {
    if (areaSeries && gData && !graphStatus) {
      console.log(gData);
      console.log("Here, this should populate the chart", areaSeries);
      areaSeries.setData(gData);
      setGraphStatus(true);
    }
  });

  return (
    <Wrapper>
      <Sidebar />
      <div className="main">
        <div className="left-section">
          <article
            className="graph card"
            ref={chartRef}
            id="chart-container"
          ></article>
          <section className="card-container">
            <article className="card info-card yellow-border">
              <h3>Total Users</h3>
              <h3 className="value">{totUsers}</h3>
            </article>
            <article className="card info-card red-border">
              <h3>Total Accounts</h3>
              <h3 className="value">{totAccounts}</h3>
            </article>
            <article className="card info-card blue-border">
              <h3>Online Accounts</h3>
              <h3 className="value">{totOnlineAccounts}</h3>
            </article>
            <article className="card info-card red-border">
              <h3>Total Balance</h3>
              <h3 className="value">${parseFloat(totBalance).toFixed(2)}</h3>
            </article>
            <article className="card info-card blue-border">
              <h3>Cash Balance</h3>
              <h3 className="value">
                ${parseFloat(totCashBalance).toFixed(2)}
              </h3>
            </article>
            <article className="card info-card yellow-border">
              <h3>Locked Balance</h3>
              <h3 className="value">
                ${parseFloat(totLockedBalance).toFixed(2)}
              </h3>
            </article>
          </section>
        </div>
        <div className="right-section">
          <AccountList />
        </div>
      </div>
    </Wrapper>
  );
};

const AccountList = () => {
  var { accountCards } = useAdminContext();

  return (
    <section className="account-list card shadow">
      <div className="cards-list">
        {accountCards &&
          accountCards.map((c) => {
            return <AccountCard account={c} key={c._id} />;
          })}
      </div>
    </section>
  );
};

const AccountCard = (props) => {
  const { account } = props;
  //console.log("Data", props);
  const { homeCards } = useAdminContext();
  let temp = homeCards.find((o) =>
    o["user_accounts"].includes(account?.account_number)
  );
  return (
    <Link to={`/user/${account?.account_number}`}>
      <article
        className={
          account?.apistatus === "valid"
            ? "account-card card shadow-sm"
            : "account-card card shadow-sm invalid-account"
        }
      >
        <div className="account-info">
          <section className="left-data">
            <p className="name">{temp ? temp.name : "Undefined"}</p>
            <p
              className={
                account?.status === "online" ? "usdt online" : "usdt offline"
              }
            >
              <span>Profit</span>{" "}
              {parseFloat(
                ((account?.account_balance -
                  account?.last_round_start_balance) *
                  100) /
                  account?.last_round_start_balance
              ).toFixed(2)}
              %
            </p>
          </section>
          <p className="acc-num">
            <span className="label2">Acc.No </span>
            <span
              className={
                account?.status === "online" ? "usdt online" : "usdt offline"
              }
            >
              {account?.account_number}
            </span>
            <span className="label2">Round SD </span>
            <span
              className={
                account?.status === "online" ? "usdt online" : "usdt offline"
              }
            >
              {account?.last_cut_off_date}
            </span>
          </p>
        </div>
      </article>
    </Link>
  );
};

/* Some color info
bgcolor: #151A22
uppercolor: #2B2F39
fontcolor: #fffefb
yellowcolor: #ff9b02
bluecolor: #526acb
*/
const Wrapper = styled.div`
  position: relative;
  background-color: #151a22;
  font-family: "Astro";
  .main {
    position: relative;
    min-height: 100vh;
    padding: 2rem 2rem 1rem 1rem;
    display: grid;
    gap: 1rem;
    .card {
      border-radius: 25px;
      background-color: #2b2f39;
      color: white;
    }
    .left-section {
      min-height: 100%;
      display: grid;
      place-items: center;
      gap: 1rem;
      .graph {
        width: 100%;
        min-height: 300px;
        background-color: black;
        overflow-x: hidden;
      }
      .card-container {
        width: 100%;
        display: flex;
        flex-wrap: wrap;
        row-gap: 1rem;
        column-gap: 2rem;
        align-items: center;
        justify-content: space-evenly;
        .info-card {
          padding: 1rem;
          width: 250px;
          min-height: 100px;
          h3 {
            text-align: center;
            font-weight: 400;
            font-size: 1.4rem;
          }
          .value {
            margin: 0;
            font-size: 1.2rem;
            color: whitesmoke;
          }
        }
        .yellow-border {
          border-left: 4px solid #ff9b02;
          color: #ff9b02;
        }
        .red-border {
          border-left: 4px solid red;
          color: red;
        }
        .blue-border {
          border-left: 4px solid #526acb;
          color: #526acb;
        }
      }
    }
    .right-section {
      .account-list {
        width: 100%;
        padding: 10px;
        .cards-list {
          display: flex;
          flex-direction: column;
          gap: 5px;
        }
        a {
          text-decoration: none;
        }
        .account-card {
          background-color: #151a22;
          border-radius: 10px;
          padding: 10px;
          .account-info {
            display: grid;
            grid-template-columns: 1fr 1fr;
            align-items: center;
            gap: 10px;
            p {
              margin: 1px;
              font-size: 1rem;
            }
            .left-data {
              .name {
                color: white;
                letter-spacing: 1px;
                font-weight: 500;
              }
            }
            .usdt {
              font-weight: 500;
              color: #1f686b;
            }
            .acc-num {
              display: grid;
              place-items: center;
            }
            .label2 {
              color: white;
              font-weight: 600;
            }
            .online {
              color: lightgreen;
            }
            .offline {
              color: red;
            }
          }
        }
        .invalid-account {
          border-color: red;
        }
      }
    }
  }

  @media screen and (min-width: 804px) {
    .main {
      margin-left: 120px;
      grid-template-columns: 2fr 1fr;
      /* max-height: 100vh; */
      .left-section {
        .graph {
          width: 100%;
          height: 100%;
          max-height: 500px;
          display: grid;
          place-items: center;
          background-color: black;
        }
        .card-container {
          .info-card {
            padding: 1rem;
            max-width: 200px;
            min-height: 100px;
            h3 {
              text-align: center;
              font-weight: 400;
              font-size: 1.4rem;
            }
            .value {
              margin: 0;
              font-size: 1.2rem;
            }
          }
          .yellow-border {
            border-left: 4px solid #ff9b02;
            color: #ff9b02;
          }
          .red-border {
            border-left: 4px solid red;
            color: red;
          }
          .blue-border {
            border-left: 4px solid #526acb;
            color: #526acb;
          }
          .value {
            color: whitesmoke;
          }
        }
      }
      .right-section {
        .account-list::-webkit-scrollbar {
          display: none;
        }
        .account-list {
          width: 100%;
          max-height: 95vh;
          padding: 10px;
          overflow-y: auto;
          display: flex;
          flex-direction: column;
          justify-content: space-between;
          .cards-list {
            display: flex;
            flex-direction: column;
            gap: 5px;
          }
          a {
            text-decoration: none;
          }
          .account-card {
            background-color: #151a22;
            border-radius: 10px;
            padding: 10px;
            .account-info {
              display: grid;
              grid-template-columns: 1fr 1fr;
              align-items: center;
              gap: 10px;
              p {
                margin: 1px;
                font-size: 1rem;
              }
              .left-data {
                .name {
                  color: white;
                  letter-spacing: 1px;
                  font-weight: 500;
                }
              }
              .usdt {
                font-weight: 500;
                color: #1f686b;
              }
              .acc-num {
                display: grid;
                place-items: center;
              }
              .label2 {
                color: white;
                font-weight: 600;
              }
              .online {
                color: lightgreen;
              }
              .offline {
                color: red;
              }
            }
          }
        }
      }
    }
  }

  @media screen and (min-width: 992px) {
    display: grid;
    place-items: center;
    * {
      max-width: 1920px;
    }
  }
`;

export default Dashboard;
