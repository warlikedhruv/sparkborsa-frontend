import React from "react";
import { useMainContext } from "./context/mainContext";
import { AdminContextProvider } from "./context/adminContext";
import { UserContextProvider } from "./context/userContext";
import styled from "styled-components";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import {
  Dashboard,
  Login,
  Landing,
  Signup,
  SingleUser,
  UserDashboard,
  PayFees,
  NewUsers,
  DisplayUser,
  VisitorDashboard,
  AddAccount,
  AddAnotherAccount,
  Agreement,
  Bots
} from "./pages";

const App = () => {
  // Authentication token
  const { token, currentUser } = useMainContext();

  //console.log("token", token);

  // If authenticated and role admin then admin routes
  if (token && currentUser?.role === "ADMIN") {
    return (
      <AdminContextProvider>
        <Wrapper>
          <Router>
            <Switch>
              <Route path="/dashboard">
                <Dashboard />
              </Route>
              <Route path="/newusers" exact>
                <NewUsers />
              </Route>
              <Route path="/user/:id">
                <SingleUser />
              </Route>
              <Route path="*">
                <Redirect to="/dashboard" />
              </Route>
            </Switch>
          </Router>
        </Wrapper>
      </AdminContextProvider>
    );
  }
  // If authenticated and role user then user routes
  else if (token && currentUser?.role === "USER") {
    return (
      <UserContextProvider>
        <Wrapper>
          <Router>
            <Switch>
              <Route
                path="/userDashboard"
                render={(props) => <UserDashboard {...props} />}
              />
              <Route path="/userAccounts/:id" exact>
                <DisplayUser />
              </Route>
              <Route path="/addAnotherAccount">
                <AddAnotherAccount />
              </Route>
              <Route path="/userAccounts/:id/payFees">
                <PayFees />
              </Route>
              <Route path="/agreement">
                <Agreement />
              </Route>
              <Route path="*">
                <Redirect to="/userDashboard" />
              </Route>
            </Switch>
          </Router>
        </Wrapper>
      </UserContextProvider>
    );
  }
  // If authenticated and role user then user routes
  else if (token && currentUser?.role === "VISITOR") {
    return (
      <Wrapper>
        <Router>
          <Switch>
            <Route
              path="/visitorDashboard"
              render={(props) => <VisitorDashboard {...props} />}
            />
            <Route path="/addAccount" exact>
              <AddAccount />
            </Route>
            <Route path="/agreement">
              <Agreement />
            </Route>
            <Route path="*">
              <Redirect to="/visitorDashboard" />
            </Route>
          </Switch>
        </Router>
      </Wrapper>
    );
  } else {
    return (
      <Wrapper>
        <Router>
          <Switch>
            <Route path="/" exact>
              <Landing />
            </Route>
            <Route path="/signup">
              <Signup />
            </Route>
            <Route path="/bots">
              <Bots />
            </Route>
            <Route path="/login" render={(props) => <Login {...props} />} />
            <Route path="/agreement">
              <Agreement />
            </Route>
            <Route path="*">
              <Redirect to="/" />
            </Route>
          </Switch>
        </Router>
      </Wrapper>
    );
  }
};

const Wrapper = styled.section``;

export default App;
